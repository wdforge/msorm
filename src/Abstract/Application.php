<?php

/**
 * Microservice
 *
 * @package    System
 * @version    1.0
 */

/**
 * Базовый класс сервиса
 */
class Abstract_Application extends Action_Object
{

    /**
     * @var Class_AutoLoader
     */
    protected $loader;

    /**
     * @var Action_Controller
     */
    protected $controller;

    /**
     * @var string
     */
    protected $controller_class;

    /**
     * @var string
     */
    protected $method_name;

    /**
     * @var Msevent_Item
     */
    protected $event;

    /**
     * @var Abstract_ViewModel
     */
    protected $viewModel;
    protected $_microservicesObserved = [];
    protected $initalized = false;
    protected static $current_service = null;
    /**
     * @var Action_Controller
     */
    protected $error_controller;
    protected static $defaultClassModel = 'Json_Model';

    /**
     * Конструктор
     *
     * @param array $configs
     * @param integer $startTime
     * @return none
     */
    function __construct($configs = [])
    {
        $this->startWorkTime();

        self::loadConfigs($configs);
        $cfg = self::getConfig();

        $this->setCurrentService(
            strtolower(
                str_replace("_Service", "", get_class($this))
            )
        );

        parent::__construct([
            'thisUrl' => isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '',
        ]);

        $this->_microservicesObserved[get_class($this)] = $this;

        if (!empty($cfg['class_loader']) && is_object($cfg['class_loader'])) {
            $this->loader = $cfg['class_loader'];
        }

        $this->init();
    }

    /**
     * Основной метод инициализации и обработки запроса
     *
     * @param $requestUri string
     * @param $routeInfo   array
     * @param $output       bool | вывод результатов через ViewModel или возврат результата без вывода
     * @param $parseGlobalRequest  bool | валидация и фильтрация параметров
     *
     * @return null
     */
    public function run($requestUri = null, $routeInfo = null, $output = true, $parseGlobalRequest = true, $format = 'json')
    {
        if ($output){
            ob_start();
        }

        $result = null;

        // определение контроллера и метода + получение передаваемых параметров через URL

        if (!$routeInfo) {
            $routeInfo = $this->getRouter()->getRoute($requestUri);
        } else {
            $this->getRouter()->setRouteInfo($routeInfo);
        }
        $this->viewModel = $this->getViewModel($format);

        if (!empty($routeInfo)) {

            $this->controller_class = Http_Router::getControllerName($routeInfo['controller']);
            $this->method_name = Http_Router::getMethodName($routeInfo['action']);
            $this->controller = $this->getController($routeInfo);

            $requestFilter = $this->controller->getRequestFilter($this->method_name);
            $this->getRequest()->createRequest($this->controller_class, $this->method_name);

            // делаем массив параметров ассоциативным и передаем в запрос
            $this->getRequest()->setUrlParams(
                $this->getRouter()->setMapData($requestFilter)
            );
            $this->getRequest()->setRequestFilter(
                $requestFilter
            );

            if ($parseGlobalRequest) {

                // обработка переданных параметров
                $this->getRequest()->filtrateParams();

                // получение get параметров
                $getData = $this->getRequest()->getFiltratedGetData();

                // установка кодировки в видовую модель
                if(!empty($getData['outcharset'])) {
                    $this->viewModel->setCharset($getData['outcharset']);
                }

                // передача управления в контроллер
                if (!is_object($this->controller)) {
                    trigger_error('Controller ' . $this->controller_class . ' not found.');

                    if (!isset(self::$config['error_hook']) || self::$config['error_hook']) {
                        $this->error_controller->setViewModel($this->viewModel);

                        // выполнение метода отсутствия страницы
                        $this->error_controller->dispatch(
                            'error404Action', $this->getRequest()->getParams(), $this->getRequest()
                        );
                    }

                } else {

                    // передача вызова в контроллер
                    if($this->controller instanceof Service_Abstract_Container) {

                        // передаём указатель на модель для вывода
                        $this->controller->getInstance()->setViewModel($this->viewModel);

                        // вызов из контейнера:
                        $callable = Service_Abstract_Container::getCallable();
                        $result = $callable($this->controller->getInstance(), $this->method_name, [
                            'params' => $this->getRequest()->getParams(true),
                            'request' => $this->getRequest()
                        ]);
                    } else {
                        // передаём указатель на модель для вывода
                        $this->controller->setViewModel($this->viewModel);

                        $result = $this->controller->dispatch(
                            $this->method_name, $this->getRequest()->getParams(true), $this->getRequest()
                        );
                    }
                }
            } else {

                $this->getRequest()->createRequest($this->controller_class, $this->method_name);
                // передача вызова в контроллер
                if($this->controller instanceof Service_Abstract_Container) {

                    // передаём указатель на модель для вывода
                    $this->controller->getInstance()->setViewModel($this->viewModel);

                    // вызов из контейнера:
                    $callable = Service_Abstract_Container::getCallable();
                    $result = $callable($this->controller->getInstance(), $this->method_name, [
                        'params' => $this->getRequest()->checkParams($routeInfo['params']),
                        'request' => $this->getRequest()
                    ]);
                } else {
                    $this->controller->setViewModel($this->viewModel);
                    $result = $this->controller->dispatch(
                        $this->method_name, $this->getRequest()->checkParams($routeInfo['params']), $this->getRequest()
                    );
                }
            }

            if (is_object($this->viewModel)) {

                $class_model = get_class($this->viewModel);

                // вывод результата
                if (!empty($this->viewModel) && is_object($this->viewModel) && ($this->viewModel instanceof Abstract_ViewModel)) {
                    if ($output) {
                        if (is_object($result) && $result instanceof Abstract_ViewModel) {
                            // вывод модели которую вернул контроллер
                            echo $result;
                        } else {
                            // вывод перевода в строку
                            echo $this->viewModel;
                        }
                    } else {
                        // запрет установки своих заголовков
                        Abstract_ViewModel::setUseHeader(false);

                        if (is_object($result) && $result instanceof Abstract_ViewModel) {
                            return $result->getProxyObject();
                        } else {
                            return $result;
                        }
                    }
                }
            } else {
                throw new Exception(sprintf("viewModel is not Object"));
            }
        }

        return $result;
    }

    public function init()
    {
        $this->initalized = true;

        if (!isset(self::$config['error_hook']) || self::$config['error_hook']) {

            // Для передачи обработки ошибок создаем контроллер.
            if (empty($this->error_controller)) {
                $this->error_controller = new Error_Controller;
            }
        }

        parent::init();
    }

    /**
     * Метод для создания экземпляра контроллера
     *
     * @param string $shortName
     * @return object | Null_Object
     */
    public function getController($routeInfo)
    {
        $cfg = self::getConfig();

        if (isset($routeInfo['service'])) {
            if (empty($cfg['primary_controller'][$this->getCurrentService()])) {
                if($routeInfo['controller']) {
                    $controller = [ucfirst($routeInfo['service']),  ucfirst($routeInfo['controller'])];
                    $class_name = Http_Router::getControllerName( $controller[0] == $controller[1] ? $controller[0] : implode('_', $controller) );
                } else {
                    $controller = [ucfirst($routeInfo['service'])];
                    $class_name = Http_Router::getControllerName( implode('_', $controller) );
                }
            } else {
                $class_name = Http_Router::getControllerName($routeInfo['controller']);
            }
        } else {

            $shortName = $routeInfo['controller'];

            if (empty($shortName)) {

                $this
                    ->viewModel
                    ->addError(
                        sprintf('Class controller is empty'),
                        __METHOD__,
                        __LINE__
                    );

                return false;
            }

            $class_name = Http_Router::getControllerName($shortName);
        }

        try {
            if (class_exists($class_name)) {
                $controller = $this->getServiceManager()->create($class_name);

                // для передачи запроса тип контроллеров должен быть подчиненным сервису.
                if ($controller instanceof Action_Controller ||
                    ($controller instanceof Service_Abstract_Container &&
                        $controller->getInstance() instanceof  Action_Controller)
                ) {
                    $this->controller = $controller;
                    return $this->controller;
                } else {
                    trigger_error('Class ' . $class_name . ' is not based on Action_Controller or Service_Abstract_Container');
                }
            } else {

                trigger_error('Class ' . $class_name . ' not found');
            }
        } catch (Exception $e) {
            trigger_error('Class ' . $class_name . ' create error: ' . $e->getMessage());
        }

        return Null_Object::create(__METHOD__);
    }

    /**
     * Метод создания видовой модели
     *
     * @param string $format
     * @return object | Null_Object
     */
    public function getViewModel($format = 'json', $params = [])
    {
        $cfg = self::getConfig();

        $classViewModel = isset($cfg['viewModel'][$format]['class']) ?
            $cfg['viewModel'][$format]['class'] :
            self::$defaultClassModel;

        if (class_exists($classViewModel)) {
            return new $classViewModel($params);
        } else {
            throw new Exception(sprintf("Cannot finded class %s", $classViewModel));
        }

    }

    /**
     * Метод получения краткого имени текущего сервиса исполняемого кода
     *
     * @param none
     * @return string
     */
    public function getCurrentService()
    {
        return self::$config['servicename'];
    }

    /**
     * Метод установки краткого имени текущего сервиса исполняемого кода
     *
     * @param string
     * @return bool
     */
    public function setCurrentService($service)
    {
        return self::$config['servicename'] = $service;
    }

}
