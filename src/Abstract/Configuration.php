<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

// shared данные
require_once dirname(__FILE__) . '/Timetest.php';

/**
 * Класс для управления настройками в виде возвращаемого массива из php файла.
 */
class Abstract_Configuration extends Abstract_Timetest {

    public static $config;

    /**
     * Метод загрузки массива путей конфигов
     *
     * @param array $configs
     * @return none
     */
    public static function loadConfigs($configs) {
        // загрузка конфига или конфигов
        if (!empty($configs) && is_array($configs)) {
            foreach ($configs as $config_path) {
                self::loadConfig($config_path);

                // вызов кода из конфига
                self::callback('_callback');
            }
        } elseif (!empty($configs) && is_string($configs)) {
            self::loadConfig($configs);

            // вызов кода из конфига
            self::callback('_callback');
        }
    }

    /**
     * Метод для выполнения callback секции  в конфиге
     *
     * @param string $shortName
     * @return string
     */
    public static function callback($section = '_callback', $drop = true) {
        if (isset(self::$config[$section]) && is_array(self::$config[$section])) {
            foreach (self::$config[$section] as $call) {
                if (is_callable($call)) {
                    try {
                        $call();
                    } catch (Exception $e) {
                        throw $e;
                    }
                }
            }
        }

        if (isset(self::$config[$section]) && is_callable(self::$config[$section])) {
            try {
                self::$config[$section]();
            } catch (Exception $e) {
                throw $e;
            }
        }

        if (isset(self::$config[$section]) && $drop) {
            unset(self::$config[$section]);
        }
    }

    /**
     * Обращение к массиву настроек
     *
     * @param null
     * @return array
     */
    public static function getConfig() {
        return self::$config ? self::$config : Null_Object::create(__METHOD__);
    }

    /**
     * Загрузка нового конфиг-массива
     *
     * @param string $filePath
     * @return array
     */
    public static function loadConfig($filePath) {
        if (is_null(self::$config)) {
            self::$config = [];
        }

        $MergeArrays = function($Arr1, $Arr2) use (&$MergeArrays)
        {
            foreach($Arr2 as $key => $Value)
            {
                if(array_key_exists($key, $Arr1) && is_array($Value))
                    $Arr1[$key] = $MergeArrays($Arr1[$key], $Arr2[$key]);
                else if (is_int($key) )
                    $Arr1[] = $Value;
                else
                    $Arr1[$key] = $Value;

            }

            return $Arr1;

        };

        if (file_exists($filePath) && is_readable($filePath)) {
            $new_config = include_once($filePath);
            if (is_array($new_config)) {
                self::$config = $MergeArrays(self::$config, $new_config);
            }
        } else {
            throw new Exception('File config not found. Path: ' . $filePath);
        }

        return self::getConfig();
    }

    /**
     * Генерация события ошибки (недописано)
     *
     * @param string $message
     * @return none
     */
    public function errorTrigger($message) {

    }

}
