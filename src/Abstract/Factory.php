<?php

/**
 * Microservice
 *
 * @package    System\Abstract\Factory
 * @version    1.0
 */

/**
 * Класс обёртка для создания нужного типа объектов
 */
abstract class Abstract_Factory extends Action_Object {

    protected $_classEntitie;

    /**
     * Основной метод получения нового объекта
     *
     * @param array
     * @return object
     */
    public function createObject(array $params) {
        try {
            if(empty($params)) {
                return null;
            }
            if (class_exists($this->getItemClass())) {

                $classObject = $this->getItemClass();
                $objectItem = $this->getServiceManager()->create($classObject, [
                    $params, $this
                        ->getServiceManager()
                        ->getRepository($classObject)
                ]);

                return $objectItem;

            }
        }
        catch(Exception $e) {
            throw $e;
        }

        return null;
    }

    protected function getItemClass() {
        return $this->_classEntitie;
    }

    protected function setItemClass($className) {
        $this->_classEntitie = $className;
    }

}
