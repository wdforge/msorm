<?php

/**
 * Microservice
 * 
 * @package    Item
 * @version    1.0
 */

/**
 * Базовый класс элемента выборки
 */
abstract class Abstract_Item extends Action_Object
    implements JsonSerializable, Db_Interface_Item_Lazy {

    /* Исходные поля объекта ActiveRecord, взятые из репозитория. Исходные поля обновляются при сохранении в репозиторий. */
    protected $_cleanData = [];
    protected $_data = [];

    /* @var Abstract_Repository $_repository */
    protected $_repository = null;

    /*
     * Дополнительные настройки объекта, метаданные выборки объекта из репозитория
     * @var array
     */
    protected $_settings = null;
    protected $_type = '';
    protected $_idProp = 'id';

    private $_lazyId = null;

    public function __construct($values = null, Abstract_Repository $repository = null, $settings = null) {
        if ($values) {
            if (!$this->_cleanData){
                $this->_cleanData = $values;
            }
            if ($values && !is_array($values) && !($values instanceof ArrayAccess) ){
                $this->_setLazyId($values);
            }else{
                $this->setFromArray($values);
            }
        }
        $this->_repository = $repository;
        if ($repository){
            $this->_idProp = $repository->getIdField();
        }else{
            throw new Exception("You must specify repository in constructor arguments to correct item creation.");
        }
        $this->_settings = $settings;
    }

    public function __get($name)
    {
        $this->_processLazy();
        return array_key_exists($name, $this->_data) ? $this->_data[$name] : null;
    }

    public function __set($name, $value)
    {
        if ($name==$this->_idProp){
            empty($this->_data[$name]) ? ($this->_data[$name] = $value)  : '';
        } else {
            $this->_data[$name] = $value;
        }
    }

    public function __unset($name)
    {
        unset($this->_data[$name]);
    }

    public function __isset($name)
    {
        $this->_processLazy();
        return array_key_exists($name, $this->_data);
    }


    /* Ленивая загрузка: путь подгрузки связанных сущностей для сущности текущего репозитория, с отложенностью.   */
    private function _setLazyId($id)
    {
        $this->_lazyId = $id;
    }

    protected function _getLazyId()
    {
        return $this->_lazyId;
    }

    private function _processLazy()
    {
        if ($id = $this->_getLazyId()){
            $this->_setLazyId(null);
            $lazyData = $this->getRepository()->fetchLazyData($id);
            /*not found items would have id = 0*/
            $lazyData ? $this->setFromArray( $lazyData ) : '';
        }
    }

    /** @virtual
     * В этой функции производится фетч всех зависимостей по ID.
     * При её объявлении работает lazyFetch. Зависимости на деле выбраны из БД не будут - а выберутся сразу пачкой при первом использовании свойств.
     * @return null
     */
    public function getLazyDependencies()
    {

    }
    /* END: Ленивая загрузка */



    public function getRepository($name = null)
    {
        if ($name){
            return $this->getServiceManager()->getRepository($name);
        }
        return $this->_repository;
    }

    /**
     * Получение массива из объекта
     *
     * @param none
     * @return array
     */
    public function jsonSerialize() {
        return $this->toArray(true);
    }

    /**
     * Получение объекта из массива
     *
     * @param array
     * @return none
     */
    public function setFromArray($values) {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Получение типа
     *
     * @param none
     * @return string
     */
    public function getType() {
        if ($this->_type) {
            return $this->_type;
        }

        $classExp = explode('_', get_class($this), 2);

        if (isset($classExp[1])) {
            return $this->_type = strtolower($classExp[1]);
        } else {
            throw new Exception("Error class name extends of Item");
        }
    }

    /**
     * Получение идентификатора
     *
     * @param none
     * @return integer
     */
    public function getId() {
        return $this->{$this->_idProp};
    }

    /*Получение названия virtual*/
    public function getTitle() {
        return  isset($this->title) ? trim($this->title) : (isset($this->name) ? trim($this->name) : '');
    }
    /**
     * Получение массива из объекта
     *
     * @param none
     * @return array
     */
    public function toArray($isJSONOutput = false) {
        $this->_processLazy();
        $arrayed = $this->_data;

        if($isJSONOutput) {
            $arrayed['type'] = $this->getType();
            $arrayed['id'] = $this->getId();
        }

        return $arrayed;
    }

    public function getSettings($setting = null) {
        if ($setting){
            return isset($this->_settings[$setting]) ? $this->_settings[$setting] : null;
        }
        return $this->_settings;
    }

    /* virtual method for save hook */
    public function onSaveBefore()
    {

    }
    /* virtual method for save hook */
    public function onSaveAfter()
    {

    }

    public function getCleanData()
    {
        return $this->_cleanData;
    }

    public function setCleanData(array $data, $to_merge = false)
    {
        if($to_merge) {
            $this->_cleanData = array_merge(is_array($this->_cleanData)?$this->_cleanData:[], $data);
        }
        else {
            $this->_cleanData = $data;
        }
    }

    public function getModifiedData($nowData = null)
    {
        $diff = [];

        foreach ($this->_data as $key=>$value){
            if (!array_key_exists($key,$this->_cleanData) || $this->_cleanData[$key]!==$value){
                $diff[$key] = $value;
            }
        }
        return $diff;
    }

    /**
     * Save an item to it's repository
     *
     * @param $params array
     * @return bool
     * @throws Exception
     */
    public function save($params = [])
    {
        if (!$this->_repository){
            throw new Exception("Item must to constructed with repository for saving");
        }
        if ($lazyId = $this->_getLazyId()){
            throw new Exception("Item is not fetched from repository with ID ". $lazyId. " . Can't save.");
        }
        if(!$this->getModifiedData() ){
            return false;
        }
        $continueSave = $this->onSaveBefore();
        if ($continueSave === false){
            return false;
        }

        $nowData =  $this->toArray();

        if ($modifiedData = $this->getModifiedData($nowData)){
            $saved = $this->_repository->save($this);
            $this->onSaveAfter();

            $this->_cleanData = $nowData;

            return $saved;
        }
        return false;
    }

    public function remove() {
        return $this->_repository->remove($this);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->_data;
    }
}
