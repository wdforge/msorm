<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Абстрактный класс для проксирования вызовов и обращения к данным
 */
class Abstract_ProxyObject extends Action_Object {

    protected $_proxyData;

    /**
     * Возвращает проксируемый объект
     *
     * @param none
     * @return array | object
     */
    public function getProxyObject() {
        return $this->_proxyData ? $this->_proxyData : Null_Object::create();
    }

    /**
     * Конструктор
     *
     * @param array | object $params
     * @return none
     */
    function __construct($params = []) {
        $this->_proxyData = $params;
    }

    /**
     * Перехват вызова модели и передача прокси-объекту
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    function __call($name, $arguments) {
        if (is_object($this->_proxyData) && method_exists($name, $this->_proxyData)) {
            try {
                $return = call_user_func_array([
                    $this->_proxyData, $name
                    ], $arguments
                );
            } catch (Exception $e) {
                throw $e;
            }
            return $return;
        } else {
            if (is_object($this->_proxyData)) {
                throw new Exception('Method ' . get_class($this->_proxyData) . '::' . $name . 'not found;');
            } else {
                throw new Exception('Proxy object is not callable');
            }
        }
    }

    /**
     * Передача установленного значения прокси-объекту
     *
     * @param string $name
     * @param mixed $value
     * @return none
     */
    function __set($name, $value) {
        if (is_object($this->_proxyData)) {
            $this->_proxyData->{$name} = $value;
        } else {
            $this->_proxyData[$name] = $value;
        }
    }

    /**
     * Возврат значения из прокси-объекта
     *
     * @param string $name     
     * @return mixed
     */
    function __get($name) {
        if (is_object($this->_proxyData)) {
            return $this->_proxyData->{$name};
        } else {
            return $this->_proxyData[$name];
        }
    }


    public function push($name, $value) {
        if(isset($this->proxyObject[$name])) {
            if (is_array($this->proxyObject[$name])) {
                $this->_proxyData[$name][] = $value;
            } else {
                $oneValue = $this->proxyObject[$name];
                $this->_proxyData[$name] = [
                    $oneValue,
                    $value
                ];
            }
        }
    }


}
