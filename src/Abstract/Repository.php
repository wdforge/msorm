<?php

/**
 * Microservice
 *
 * @package    System\Abstract\Repository
 * @version    1.0
 */

/**
 * Базовый класс для обозначения структуры репозитория
 */
abstract class Abstract_Repository extends Action_Object
    implements
        Db_Interface_Repository_ItemFieldId,
        Db_Interface_Repository_ItemClass,
        Db_Interface_Repository_ItemSettings
{

    protected $itemSettings;
    protected $itemClass;
    protected $_idKey = 'id'; /* may redefine */

    public function getItemClass()
    {
        return $this->itemClass;
    }

    public function setItemClass($itemClass)
    {
        $this->itemClass = $itemClass;
    }

    public function setItemSettings($itemSettings) {
        $this->itemSettings = $itemSettings;
        return $this;
    }

    public function getItemSettings() {
        return $this->itemSettings;
    }

    /**
     *  Основной метод выборки и трансформации в объекты
     *
     * @param   $params
     *    array   $params['class']   - таблица
     *    array   $params['table']   - таблица
     *    array   $params['fields']  - заполняемые свойства
     *    integer $params['offset']  - от
     *    integer $params['limit']   - до
     *    array   $params['where'] - условия выборки
     *    array   $params['order']    - сортировки полей
     *    array   $params['likes']   - похожести
     *    array   $params['joins']   - присоединения
     *
     * @return RepositoryResult
     **/
    abstract public function findAll($params, $only_rows_count = false);

    /** @var array[]  $_createdItems */
    private static $_createdItems = [];

    protected function _saveRealFetchedItemsToCache($item, $ids)
    {
        $items = is_array($item) ? $item  : [$item];
        foreach($ids as $id) {
            $found = false;
            foreach ($items as $it) {
                if ($it && $it->{$this->getIdField()} == $id){
                    self::$_createdItems[get_class($this)][$id] = $it;
                    if ($this->isLazyFetch()){
                        $it->getLazyDependencies();
                    }
                    $found = true;
                }
            }
            if (!$found){
                self::$_createdItems[get_class($this)][$id] = null;
            }
        }
    }

    /* Ленивая загрузка: метод подгрузки связанных сущностей для сущности текущего репозитория, с отложенностью.   */
    protected $_isLazyFetch = false;
    /** @var array[]  $lazyItems */
    protected static $_lazyItems = [];

    public function isLazyFetch()
    {
        return $this->_isLazyFetch;
    }
    public function setLazyFetch($enable = true)
    {
        $this->_isLazyFetch = $enable;
        return $this;
    }



    public function fetchLazyData($id)
    {
        if (!array_key_exists($id , isset(self::$_lazyItems[get_class($this)]) ? self::$_lazyItems[get_class($this)] : [])){
            throw new Exception("Lazy item had not prefetched");
        }else if (is_array( self::$_lazyItems[get_class($this)][$id] ) ){
            return self::$_lazyItems[get_class($this)][$id];
        }else{
            $idsForLazyFetch = [];

            foreach(self::$_lazyItems[get_class($this)] as $_id=>$lazy){
                if (!is_array($lazy)){
                    $idsForLazyFetch[] = $_id;
                }
            }

            $realItems = $this->_getByID($idsForLazyFetch);

            foreach ($idsForLazyFetch as $_id){
                if (isset($realItems[$_id]) && $realItem = $realItems[$_id]){
                    self::$_lazyItems[get_class($this)][$_id] = $realItem->toArray();
                }else{
                    self::$_lazyItems[get_class($this)][$_id] = [$this->getIdField() => 0];
                }
            }
            return self::$_lazyItems[get_class($this)][$id];
        }
    }
    /* END: Ленивая загрузка */


    /**
     * основной метод выборки одного объекта по ID
     *
     * @param integer $id
     * @return Abstract_Item|null
     **/
    public function getByID($id){
        /*Валидация $id*/
        if (is_array($id)){
            $ids = array_filter(array_unique($id));
            $isMultiQuery = true;
        }else{
            $ids = [$id];
            $isMultiQuery = false;
        }

        if (!$id || !$ids) return $isMultiQuery ? []  : null;

        /*Поиск итем-ов в run-кеше*/
        $runCached = [];
        foreach($ids as $id) {
            if (array_key_exists($id, isset(self::$_createdItems[get_class($this)]) ? self::$_createdItems[get_class($this)] : [] ) ){
                $runCached[$id] = self::$_createdItems[get_class($this)][$id];
            }
        }
        $ids = array_diff($ids, array_keys($runCached));
        if (!$ids){
            return $isMultiQuery ? array_filter($runCached) : array_shift($runCached);
        }
        /*Фейковая выборка, если в репе-вызывателе, или итеме-вызывателе isLazyFetch */
        $loadedRepos = $this->getServiceManager()->getLoadedRepositories();
        foreach (debug_backtrace() as $i=>$one) {
            if (!empty($one['class'])
                && $i
                && isset($loadedRepos[$one['class']])
                && $loadedRepos[$one['class']]->isLazyFetch()) {

                $itemClass = $this->itemClass;

                $items = [];
                foreach($ids as $id){
                    $lazyRec = isset(self::$_lazyItems[ get_class($this) ][$id]) ? self::$_lazyItems[ get_class($this) ][$id] : false;

                    if ($lazyRec instanceof Abstract_Item){
                        $items[] = $lazyRec;
                    }else{
                        $items[] = $item = $this->getServiceManager()->create($itemClass, [is_array($lazyRec) ? $lazyRec /*already fetched*/ : $id /*not fetched yet*/,
                            $this,
                            $this->getItemSettings()]);
                        if (!is_array($lazyRec)){
                            self::$_lazyItems[ get_class($this) ][$id] = $item;
                        }
                    }
                }

                return $isMultiQuery ? array_filter($items + $runCached)  : array_shift($items);
            }
        }

        /*Реальая выборка в репе*/
        $item = $this->_getByID($isMultiQuery ? $ids : array_shift($ids) );

        /*Сохранение результатов выборки в run-кеш - вызов здесь дублируется для записи ещё и null-ов*/
        $this->_saveRealFetchedItemsToCache($item, $ids);

        /*Возврат результатов реальной выборки*/
        return $isMultiQuery ? array_filter($item + $runCached) : $item;
    }

    protected abstract function _getByID($id);

    /**
     * метод добавления одной записи
     *
     * @param array $params
     * @return integer
     **/
    public function insertRow(array $params){}

    /**
     * метод обновления одной записи
     *
     * @param integer $id
     * @param array $params
     * $return bool
     **/
    abstract public function updateRow($id, array $params);

    /**
     * метод получения ключевого поля
     *
     * @param none
     * $return string
     **/
    public function getIdField()
    {
        return $this->_idKey;
    }

    /**
     * метод установки ключевого поля
     *
     * @param string
     * $return none
     **/
    public function setIdField($idField)
    {
        $this->_idKey = $idField;
    }

    /**
     * поддержка транзакций
     **/
    /* @return bool */
    abstract public function isTransaction();
    /* @return $this */
    abstract public function beginTransaction();
    /* @return $this */
    abstract public function commit();
    /* @return $this */
    abstract public function rollback();
    public static $hasDispatchError = false;
    public function __destruct() {
        /* Autocommit non-commited changes */
        if ( $this->isTransaction()){
            if (self::$hasDispatchError){
                $this->rollback();
            }else{
                $this->commit();
            }
        }
    }

    /**
     * Метод сохранения объекта в хранилище
     *
     * @param Abstract_Item
     * $return bool
     **/
    abstract public function save(Abstract_Item $object);


    /**
     * Метод удаления объекта из хранилища
     *
     * @param Abstract_Item
     * $return bool
     **/
    abstract public function remove(Abstract_Item $item);


    /**
     * Создание итема для таблицы, который затем можно сохранить, после чего произойдёт запись в БД
     * @return Abstract_Item
    */
    public function createItem($itemData = [])
    {
        $itemClass = $this->itemClass;
        /* @var Abstract_Item $item*/
        $item = new $itemClass([] /*важно в аргументах передать пустоту, чтобы пометить, что ряд нужно вставить*/,
            $this,
            $this->getItemSettings()
        );
        if ($itemData){
            $item->setFromArray($itemData);
        }
        return $item;
    }




    /** @var array[] Логгирование всех произведённых запросов */
    protected static $queryMetadata = [];

    protected function  logQuery($query, $time){
        $key = get_class($this);

        self::$queryMetadata[$key][$query] = ['time' => $time];

        if (count(self::$queryMetadata[$key]) > 1000){
            /*чтобы не забивалась оперативка при долгоработающих скриптах.*/
            array_shift(self::$queryMetadata[$key]);
        }
    }

    /**Возвращает все произведённые запросы
     *
     * @param null|string $key
     * @return array|array[]
     */
    public static function getQueryLog($key = null){
        $total = 0;
        foreach(self::$queryMetadata as $repo){
            foreach($repo as $logRec){
                $total+= $logRec['time'];
            }
        }
        return array_merge( $key ? self::$queryMetadata[$key] : self::$queryMetadata , ['total' => $total]);
    }

    public function quote($string) {
        return addslashes( $string);
    }

}
