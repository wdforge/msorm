<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс для абстракции возвращаемых из выборки значений в виде объекта и массива одновременно
 */
abstract class Abstract_RepositoryResult extends ArrayObject {
    
}
