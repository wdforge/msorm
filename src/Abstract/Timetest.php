<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Базовый класс счетчика времени 
 */
class Abstract_Timetest {

    // время запуска
    protected static $startTime;

    /**
     * Метод установки времени запуска счетчика
     *
     * @param none
     * @return none
     */
    public static function startWorkTime() {
        static::$startTime = microtime(true);
    }

    /**
     * Метод получения времени работы счетчика
     *
     * @param none
     * @return integer 
     */
    public static function getWorkTime() {

        if (static::$startTime) {
            return microtime(true) - static::$startTime;
        }

        return false;
    }

}
