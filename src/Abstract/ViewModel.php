<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Базовый класс видовой модели
 */
class Abstract_ViewModel extends Abstract_ProxyObject {

    static public $is_header = true;
    protected $encoding;

    /**
     *  Установка кодировки контента
     *
     * @param $charset
     */
    public function setCharset($charset = "utf8") {
        $this->encoding = $charset;
        return $this;
    }

    public function setCharsetProxyObject($charset, $proxyObject = null) {

        if (!empty($this->encoding)) {
            $proxyObject = $proxyObject? $proxyObject: $this->getProxyObject();

            if (is_array($proxyObject) && !empty($proxyObject)) {
                foreach ($proxyObject as $key => &$objItem) {
                    if (is_object($objItem) && $objItem instanceof Abstract_Item) {
                        $proxyObject[$key] = $this->setCharsetProxyObject($charset, $objItem);
                    }
                }
            }
            elseif(!empty($proxyObject) && $proxyObject instanceof Abstract_Item) {

                $data = $proxyObject->getData();

                foreach ($data as $name => $value) {

                    if (is_string($value)) {
                        $proxyObject->{$name} = mb_convert_encoding($value, $charset);
                    }

                    if(is_object($value) && $value instanceof Abstract_Item) {
                        $proxyObject->{$name} = $this->setCharsetProxyObject($charset, $value);
                    }
                }

                return $proxyObject;
            }
        }
    }

    public static function setUseHeader($useHeader) {
        self::$is_header = $useHeader;
    }

    public static function sendHeader($header) {
        if(self::$is_header) {
            header($header);
        }
    }

    public function getResult() {
        return isset($this->result) ? $this->result : null ;
    }

    public function setResult($result) {
        $this->result = $result;
        return $this;
    }

    /* справочная/отладочная иноформация */
    public function addError($text = "", $method = null, $line = null) {
        if (!isset($this->error)){
            $this->error = [];
        }
        $errors = $this->error;
        if($text && $method && $line) {
            $errors[] = sprintf("%s, Method: %s, Line: %s", $text, $method, $line);
        }
        elseif($text && $method) {
            $errors[] = sprintf("%s, Method: %s", $text, $method);
        }
        elseif($text && $line) {
            $errors[] =  sprintf("%s, Line: %s", $text, $line);
        }
        elseif($text) {
            $errors[] = $text;
        }
        $this->error = $errors;
        return $this;
    }

    /* справочная/отладочная иноформация */
    public function workStateTime() {
        $this->time = $this->getWorkTime();
        return $this;
    }

    /* справочная/отладочная иноформация */
    public function setAction($action = "") {
        $this->action = $action;
        return $this;
    }

}

