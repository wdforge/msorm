<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс для управления доступом
 */
class Access_Manager extends Abstract_Configuration {

    protected $_session;
    protected $service_manager;

    function __construct() {
        $this->service_manager = new Service_Manager();
    }

    /**
     *
     * @param none
     * @return Service_Manager
     */
    private function getServiceManager() {
        return $this->service_manager;
    }

    /**
     * Вход через хэш
     *
     * @param string $hash Токен клиента
     * @return null
     */
    public function enter($apiKey) {

        if ($this->exists($apiKey)) {
            $this->_session = new User_Session($apiKey);
            return true;
        }

        return false;
    }

    /**
     * Проверка на доступность по токену или id пользователя
     * 
     * @param string $hash Токен клиента
     * @return bool
     */
    public function exists($apiKey, $id = null) {
        $found = false;

        if ($apiKey && !$id) {
            $result = $this->getServiceManager()
                ->getRepository('Item_User')
                ->getByApiKey($apiKey);

            if (!empty($result)) {

                $found = true;
            }
        }


        if (!$apiKey && $id) {
            if ($this->getServiceManager()
                    ->getRepository('Item_User')
                    ->getById($id)) {

                $found = true;
            }
        }

        if ($apiKey && $id) {
            if ($this->getServiceManager()
                    ->getRepository('Item_User')
                    ->getByID($id)) {

                if ($this->getServiceManager()
                        ->getRepository('Item_User')
                        ->getByApiKey($apiKey)) {

                    $found = true;
                }
            }
        }

        return $found;
    }

    /**
     * Проверка на вход
     * 
     * @param null
     * @return bool
     */
    public function isLogged() {
        if (is_object($this->_session)) {
            return true;
        }

        return false;
    }

    /**
     * Выход пользователя
     * 
     * @param null
     * @return unknown_type
     */
    public function logout() {
        unset($this->_session);
    }

    /**
     * Данные пользователя
     * 
     * @param null
     * @return User_Session
     */
    public function getUserData() {
        return $this->_session;
    }

}
