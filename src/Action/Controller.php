<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Базовый класс контроллера для вызова сервисом через ЧПУ
 */
class Action_Controller extends Action_Object {

    protected $_requestFilter = [];
    protected $_viewModel;

    const BASE_CLASS = 'Action_Controller';

    /**
     * Метод получения отклика от контроллера сервиса и времени работы
     *
     * @param none
     * @return Json_Model
     */
    public function PingAction() {
        return new Json_Model([
            'Connect' => 'OK',
            'time' => self::getWorkTime()
        ]);
    }

    /**
     * Метод получения фильтра по методу передачи данных
     *
     * @param string $method
     * @return array
     */
    public function getRequestFilter($method = '') {
        if (!empty($method) && !empty($this->_requestFilter[$method])) {

            // добавление необязательного параметра кодировка (кот. можно передать в запросе)
            if(!isset($this->_requestFilter[$method]['__outcharset'])) {
                $this->_requestFilter[$method]['outcharset'] = [
                    'from' => 'GET',
                    'type' => 'string',
                    'regexp' => '/[a-zA-Z0-9\-_\.]+/',
                    'default' => '',
                    'error' => 'Request param "outcharset" is not valid set.',
                ];
            }

            return $this->_requestFilter[$method];
        } else {
            return [];
        }

        return $this->_requestFilter;
    }

    /**
     * Метод обработки ошибки отсутствия метода 
     *
     * @param string $request
     * @param string $method
     * @return Json_Model
     */
    public function error404Action($request, $method = "") {
        header("HTTP/1.0 404 Not Found");

        return new Json_Model([
            'error' => 'Method "' . $method . '" not found.',
            'time' => self::getWorkTime()
        ]);
    }

    /**
     * Метод для передачи обработки вызова к конкретный класс
     * 
     * @param string        $method
     * @param array         $params
     * @param Http_Request  $request
     * @return Json_Model
     */
    public function dispatch($method, $params, $request) {

        $class_name = str_replace('_Controller', '', get_class($this));
        $result = null;

        if (method_exists($this, $method)) {

            try {

                // убираем часть имени чтобы название эвента было короче
                // запуск события ДО запуска метода
                $beforeEventName = 'Before' . $class_name . 'Service' . ucfirst($method);
                $this->getEventManager()->triggerEvent($beforeEventName);

                // есть предопределенные схемой параметры, 
                // передача их как аргументов метода.
                if (!empty($params) && !$this->getRequest()->isNeedReturnRequest()) {
                    // передача вызова контроллеру
                    $result = call_user_func_array(
                        [
                        $this, $method
                        ], array_values(
                            $params
                        )
                    );
                }

                // передача объекта Request в качестве параметра
                else {
                    // передача вызова контроллеру
                    $result = call_user_func_array(
                        [
                        $this, $method
                        ], [
                        $request
                        ]
                    );
                }

                // запуск события ПОСЛЕ запуска метода
                $afterEventName = 'After' . $class_name . 'Service' . ucfirst($method);
                $this->getEventManager()->triggerEvent($afterEventName);

            } catch (\Exception $e) {
                Abstract_Repository::$hasDispatchError = true;
                throw $e;
            }
        } else {

            /**
             * обработка отсутствующего метода 
             * передача вызова контроллеру на метод error404Action
             */
            $result = call_user_func_array(
                [
                $this, 'error404Action'
                ], [
                $request,
                get_class($this) . '::' . $method
                ]
            );

            // запуск события ПОСЛЕ запуска метода
            $afterEventName = 'AfterError404Action' . $class_name . 'Service' . ucfirst($method);
            $this->getEventManager()->triggerEvent($afterEventName);
        }

        $event = $this->getEventManager()->getLastEvent();

        // устанавливаем что мы будем выводить (результат то что вернул метод)
        // если вернул ViewModel то просто возвращаем чтобы можно было заменить потом в Application.
        if(!(is_object($result) && $result instanceof Abstract_ViewModel)) {
            $this->getViewModel()->setResult($result);
        }else if ($result == $this->getViewModel() && !$this->getViewModel()->getResult()){
            $result->setResult(null);
        }

        // рузультат авто-событий (на вызов метода)
        if (!is_null($event->result)) {
            $this->getViewModel()->eventResult = $event->result;
        }

        return $result;
    }

    /**
     * Метод установления рабочей видовой модели
     *
     * @param Abstract_ViewModel $viewModel
     * @return none
     */
    public function setViewModel(Abstract_ViewModel $viewModel) {

        if(is_object($viewModel)) {
            $this->_viewModel = $viewModel;
            return true;
        }

        return false;
    }

    /**
     * Метод получения видовой модели
     *
     * @return Abstract_ViewModel
     */
    public function getViewModel() {
        return $this->_viewModel;
    }
}
