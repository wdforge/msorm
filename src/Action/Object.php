<?php

/**
 * Microservice
 *
 * @package    System
 * @version    1.0
 */

/**
 * Базовый класс прародитель множества порожденных объектов (Контроллеры, Модели, Приложения).
 * Основное назначение объединение в себе необходимых интерфейсов вроде Service_Manager.
 */
class Action_Object extends Abstract_Configuration
{

    // упр. модулями
    /* @var Module_Loader $service_manager */
    protected static $module_manager;
    // упр. доступом
    /* @var Access_Manager $service_manager */
    protected static $access_manager;
    // упр. зависимостями
    /* @var Service_Manager $service_manager */
    protected static $service_manager;
    // упр. событиями
    /* @var Msevent_Manager $event_manager */
    protected static $event_manager;
    // упр. запросом
    /* @var  Http_Request $request */
    protected static $request;
    // упр. навигацией
    /* @var  Http_Router $router */
    protected static $router;
    // упр. ошибками
    /* @var  Error_Manager $error_manager */
    protected static $error_manager;

    function __construct($params = null)
    {

        if (!self::$error_manager) {
            self::$error_manager = new Error_Manager;
            self::$error_manager->init();
        }
        // управление событиями
        if (!self::$event_manager) {
            self::$event_manager = new Msevent_Manager;
        }

        // управление зависимостями
        if (!self::$service_manager) {
            self::$service_manager = new Service_Manager;
        }

        // управление доступом
        if (!self::$access_manager) {
            self::$access_manager = new Access_Manager;
        }

        // загрузчик модулей
        if (!self::$module_manager) {
            self::$module_manager = new Module_Loader;
        }

        // управление передачей данных
        if (!self::$router) {
            self::$router = new Http_Router;
        }

        // управление передачей данных
        if (!self::$request) {
            $info = self::$router->getRouteInfo();

            if (!empty($info)) {
                self::$request = new Http_Request(
                    $info['controller'], $info['action'], $info['params']
                );
            }
        }
    }

    /* @return Msevent_Manager */
    public function getEventManager()
    {
        return self::$event_manager ? self::$event_manager : Null_Object::create(__METHOD__);
    }

    /* @return Service_Manager */
    public function getServiceManager()
    {
        return self::getStaticServiceManager();
    }

    public static function getStaticServiceManager()
    {
        return self::$service_manager ? self::$service_manager : Null_Object::create(__METHOD__);
    } 

    /* @return Access_Manager */
    public function getAccessManager()
    {
        return self::$access_manager ? self::$access_manager : Null_Object::create(__METHOD__);
    }

    /* @return  Module_Loader */
    public function getModuleManager()
    {
        return self::$module_manager ? self::$module_manager : Null_Object::create(__METHOD__);
    }

    /* @return  Http_Router */
    public function getRouter()
    {
        return self::$router ? self::$router : Null_Object::create(__METHOD__);
    }

    /* @return  Http_Request */
    public function getRequest()
    {
        return self::$request ? self::$request : Null_Object::create(__METHOD__);
    }

    public function init()
    {

        $cfg = self::getConfig();

        // загрузка общих модулей (доп. путей)
        if (!empty($cfg['modules']) && is_array($cfg['modules']) && isset(self::$module_manager) && is_object(self::$module_manager)) {
            self::$module_manager->LoadModules($cfg['modules']);
        }

        $this->getServiceManager()->set('module', $this->getModuleManager());
        $this->getServiceManager()->set('modules', $cfg['modules']);
        $this->getServiceManager()->set('domains', include MS_ROOT . '/domains.php');
    }

}
