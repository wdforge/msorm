<?php
use Symfony\Component\Cache\Adapter\AbstractAdapter;

class Cache_Symfony_Adapter_MemcacheAdapter extends AbstractAdapter
{
    use Cache_Symfony_Traits_MemcacheTrait;

    protected $lifetime;
    protected $namespace;
    protected $maxIdLength = 250;

    public function __construct(\Memcache $client, $namespace = '', $defaultLifetime = 0)
    {
        $this->init($client, $namespace, $defaultLifetime);
    }

    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, $lifetime)
    {
        foreach($values as $key => $value) {
            $this->client->set($key, $value, MEMCACHE_COMPRESSED, $lifetime);
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
        return $this->client->get($ids);
    }

    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
        return false !== $this->client->get($id);
    }

    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
        foreach ($ids as $id) {
            $this->client->delete($id);
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
        return $this->client->flush();
    }

}
