<?php

use Symfony\Component\Cache\Exception\CacheException;
use Symfony\Component\Cache\Exception\InvalidArgumentException;
use Symfony\Component\Cache\Traits\AbstractTrait;
trait Cache_Symfony_Traits_MemcacheTrait
{
    private $client;

    public static function isSupported()
    {
        return extension_loaded('memcache');
    }

    private function init(\Memcache $client, $namespace, $defaultLifetime)
    {
        if (!static::isSupported()) {
            throw new CacheException('Memcache is required');
        }

        parent::__construct($namespace, $defaultLifetime);
        $this->client = $client;
    }

    public static function createConnection($servers, array $options = array())
    {
        if (is_string($servers)) {
            $servers = array($servers);
        }
        elseif (!is_array($servers)) {
            throw new InvalidArgumentException(sprintf('MemcacheAdapter::createClient() expects array or string as first argument, %s given.', gettype($servers)));
        }

        if (!static::isSupported()) {
            throw new CacheException('Memcache is required');
        }

       // set_error_handler(function ($type, $msg, $file, $line) { throw new \ErrorException($msg, 0, $type, $file, $line); });

        try {
            $client = new \Memcache;

            // parse any DSN in $servers
            foreach ($servers as $i => $dsn) {

                if (is_array($dsn)) {
                    continue;
                }

                if (0 !== strpos($dsn, 'memcache://')) {
                    throw new InvalidArgumentException(sprintf('Invalid Memcache DSN: %s does not start with "memcache://"', $dsn));
                }

                $params = preg_replace_callback('#^memcache://(?:([^@]*+)@)?#', function ($m) use (&$username, &$password) {
                    if (!empty($m[1])) {
                        list($username, $password) = explode(':', $m[1], 2) + array(1 => null);
                    }
                    return 'file://';
                }, $dsn);


                if (false === $params = parse_url($params)) {
                    throw new InvalidArgumentException(sprintf('Invalid Memcache DSN: %s', $dsn));
                }

                if (!isset($params['host']) && !isset($params['path'])) {
                    throw new InvalidArgumentException(sprintf('Invalid Memcache DSN: %s', $dsn));
                }

                if (isset($params['path']) && preg_match('#/(\d+)$#', $params['path'], $m)) {
                    $params['weight'] = $m[1];
                    $params['path'] = substr($params['path'], 0, -strlen($m[0]));
                }

                $params += array(
                    'host' => isset($params['host']) ? $params['host'] : $params['path'],
                    'port' => isset($params['host']) ? 11211 : null,
                    'weight' => 0,
                );

                if (isset($params['query'])) {
                    parse_str($params['query'], $query);
                    $params += $query;
                }

                $servers[$i] = array($params['host'], $params['port'], $params['weight']);
            }

            foreach($servers as $server) {
                $client->addServer($server[0], $server[1], 0);
            }

            return $client;

        } finally {
            restore_error_handler();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, $lifetime)
    {
        return $this->checkResultCode($this->client->setMulti($values, $lifetime));
    }

    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
        return $this->checkResultCode($this->client->getMulti($ids));
    }

    /**
     * {@inheritdoc}
     */
    protected function doHave($id)
    {
        return false !== $this->client->get($id) || $this->checkResultCode(\Memcached::RES_SUCCESS === $this->client->getResultCode());
    }

    /**
     * {@inheritdoc}
     */
    protected function doDelete(array $ids)
    {
        $ok = true;
        foreach ($this->checkResultCode($this->client->deleteMulti($ids)) as $result) {
            if (\Memcached::RES_SUCCESS !== $result && \Memcached::RES_NOTFOUND !== $result) {
                $ok = false;
            }
        }

        return $ok;
    }

    /**
     * {@inheritdoc}
     */
    protected function doClear($namespace)
    {
        return $this->checkResultCode($this->client->flush());
    }

    private function checkResultCode($result)
    {
        $code = $this->client->getResultCode();

        if (\Memcached::RES_SUCCESS === $code || \Memcached::RES_NOTFOUND === $code) {
            return $result;
        }

        throw new CacheException(sprintf('MemcachedAdapter client error: %s.', strtolower($this->client->getResultMessage())));
    }
}
