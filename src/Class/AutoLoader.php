<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */
require_once dirname(__FILE__) . '/../Abstract/Configuration.php';

/**
 * Класс для автоматическго инклуда файлов по имени класса.
 * Ищет нужный фаил по объявленным путям в конфиге, секция "autoload_paths"
 */
class Class_AutoLoader extends Abstract_Configuration {

    private $_fileExtension = '.php';
    private $_namespace;
    private $_namespaceSeparator = '\\';

    function __construct($configs) {
        self::loadConfigs($configs);
        self::$config['class_loader'] = $this;
    }

    /**
     * Устанавливает разделитель пространства имен, используемый для классов
     * 
     * @param string $sep The separator to use.
     */
    public function setNamespaceSeparator($sep) {
        $this->_namespaceSeparator = $sep;
    }

    /**
     * Возвращает разделитель пространства имен, используемое классов
     *
     * @return none
     */
    public function getNamespaceSeparator() {
        return $this->_namespaceSeparator;
    }

    /**
     * Устанавливает базовый путь который включает все файлы классов
     * 
     * @param string $includePath
     */
    public function setIncludePath($includePath) {
        $this->_includePath = $includePath;
    }

    /**
     * Добавляет путь для поиска классов
     *
     * @return string $includePath
     */
    public function addIncludePath($path) {
        self::$config['autoload_paths'][] = $path;
    }

    /**
     * Возвращает пути пути поиска файлов классов
     *
     * @return string $includePath
     */
    public function getIncludePaths() {
        return self::$config['autoload_paths'];
    }

    /**
     * Задает расширение для файлов классов
     * 
     * @param string $fileExtension
     */
    public function setFileExtension($fileExtension) {
        $this->_fileExtension = $fileExtension;
    }

    /**
     * Получает расширение файлов классов
     *
     * @return string $fileExtension
     */
    public function getFileExtension() {
        return $this->_fileExtension;
    }

    /**
     * Устанавливает этот загрузчик классов в стек автозагрузки SPL.
     */
    public function register() {
        spl_autoload_register(array($this, 'autoload'));
    }

    /**
     * Удаляет загрузчик классов из SPL стека автозагрузки.
     */
    public function unregister() {
        spl_autoload_unregister(array($this, 'autoload'));
    }

    public function autoload($className) {

        if (is_array($this->getIncludePaths())) {
            foreach ($this->getIncludePaths() as $path) {
                if (empty($path)) {
                    continue;
                }

                if (!file_exists($path)) {
                    trigger_error(sprintf('Directory "%s" is not found.', $path));
                    continue;
                }

                if ($path[strlen($path) - 1] != DIRECTORY_SEPARATOR) {
                    $path .= DIRECTORY_SEPARATOR;
                }

                if (null === $this->_namespace || $this->_namespace . $this->_namespaceSeparator === substr($className, 0, strlen($this->_namespace . $this->_namespaceSeparator))) {

                    $fileName = '';
                    $namespace = '';

                    if (false !== ($lastNsPos = strripos($className, $this->_namespaceSeparator))) {

                        $namespace = substr($className, 0, $lastNsPos);
                        $className = substr($className, $lastNsPos + 1);

                        $fileName = str_replace($this->_namespaceSeparator, DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
                    }

                    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . $this->_fileExtension;

                    if (file_exists($path . DIRECTORY_SEPARATOR . $fileName)) {
                        require ($path !== null ? $path . DIRECTORY_SEPARATOR : '') . $fileName;
                    } else {
                        continue;
                    }
                }
            }
        } else {
            trigger_error('\$cfg[\'autoload_paths\'] is not array.');
        }
    }

}
