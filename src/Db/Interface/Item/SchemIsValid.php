<?php

interface Db_Interface_Item_SchemIsValid {
    public function isValid();
    public function toValid();
}