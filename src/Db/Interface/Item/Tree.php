<?php

interface Db_Interface_Item_Tree extends Db_Interface_Repository_ItemParentFieldId {

    public function getChilds($params = []);
    public function hasChilds($params = []);

}