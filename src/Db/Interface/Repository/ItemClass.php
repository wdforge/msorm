<?php

interface Db_Interface_Repository_ItemClass {

    public function getItemClass();
    public function setItemClass($itemClass);

}