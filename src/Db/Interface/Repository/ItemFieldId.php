<?php


interface Db_Interface_Repository_ItemFieldId {

    /**
     * основной метод выборки одного объекта по ID
     *
     * @param integer $id
     * @return Abstract_Item|null
     **/
    public function getByID($id);

    /**
     * метод получения ключевого поля
     *
     * @param none
     * $return string
     **/
    public function getIdField();


    /**
     * метод установки ключевого поля
     *
     * @param string
     * $return none
     **/
    public function setIdField($idField);

}