<?php

interface Db_Interface_Repository_ItemParentFieldId {

    /**
     * метод выборки предка текущего объекта
     *
     * @param integer $id
     * @return Abstract_Item|null
     **/
    public function getParent();

    /**
     * метод получения ключевого поля
     *
     * @param none
     * $return string
     **/
    public function getParentIdField();


    /**
     * метод установки ключевого поля
     *
     * @param string
     * $return none
     **/
    public function setParentIdField($idParentField);

}