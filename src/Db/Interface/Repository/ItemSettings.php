<?php

interface Db_Interface_Repository_ItemSettings {

    public function getItemSettings();
    public function setItemSettings($itemSettings);

}