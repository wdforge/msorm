<?php

interface Db_Interface_Repository_TreeList extends Db_Interface_Repository_ItemParentFieldId {

    /**
     * @param integer $elementId
     * @return Tree_Interface_Item []
     **/
    public function getChilds($elementId, $params = [], $toArray = false);
    public function getArrayItems();
    public function getRootItems($params = []);
}