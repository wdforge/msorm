<?php

/**
 * Microservice
 * 
 * @package    System\Db\Solr\Repository
 * @author     Alexandr Goncharov <sunnydayd@mail.ru>
 *
 * @version    1.0
 */
class Db_Solr_Repository extends Abstract_Repository {

    protected $_generalCollection = null; /* for redefine */
    protected $_defaultResponseLimit = 30;
    public $solr = null;
    protected $lastSolrQuery = '';
    protected $lastResponseTime = -1;
    protected $lastResponseNumFound = -1;
    protected $lastResponseStatus = -1;
    protected $is_error = false;

    public function getLastSolrQuery() {
        return $this->lastSolrQuery;
    }

    public function getLastResponseTime() {
        return $this->lastResponseTime;
    }

    public function getLastResponseNumFound() {
        return $this->lastResponseNumFound;
    }

    public function getLastResponseStatus() {
        return $this->lastResponseStatus;
    }

    public static $solrClients = [];

    public function __construct(array $params) {
        if(isset($params['field_id'])) {
            $this->setIdField($params['field_id']);
        }

        if(isset($params['connect'])) {
            if ($this->_generalCollection && empty($params['connect']['path'])){
                $params['connect']['path'] = $this->_generalCollection;
            }

            if (empty($params['connect']['wt'])){
                $params['connect']['wt'] = 'json';
            }
            if (empty($params['connect']['indent'])){
                $params['connect']['indent'] = true;
            }
            $clientKey = $params['connect']['hostname'].$params['connect']['port'].$params['connect']['path'];
            $this->solr = isset(self::$solrClients[$clientKey])  ? self::$solrClients[$clientKey]  : (self::$solrClients[$clientKey] = new \SolrClient($params['connect']));
        }
        else {

            trigger_error(sprintf("Please set \"connect\" section in config for repository: %s ", get_class($this)));
        }
    }

    /* Support params:

      query => string | array(
      string1,
      string2,
      array3 => array(<query ?> => <value_to_escape_in?>),
      array4 => array(<queryPart1 ?> => <value_to_escape_in?>, <queryPart2 ?> => <value_to_escape_in ?>),
      ..)
      limit => int
      offset => int
      fields => array | string
      order  => string "<field> ASC|DESC"

        @return Abstract_Item[]
     * */

    public function findAll($params = [], $only_rows_count = false) {

        $q = new \SolrQuery('*:*');

        if (isset($params['limit'])) {
            $q->setRows((int) $params['limit']);
        } elseif(!$only_rows_count) {
            $q->setRows($this->_defaultResponseLimit);
        } else {
            $q->setRows(0);
        }

        if (isset($params['offset'])) {
            $q->setStart((int) $params['offset']);
        }

        if (!empty($params['fields'])) {
            $fields = is_string($params['fields']) ? explode(',', $params['fields']) : $params['fields'];
            foreach ($fields as $field) {
                $q->addField(trim($field));
            }
        }
        $query = '*:*';

        $setQuery = function($query) use ($params, $q){
            if(isset($params['hasFieldBoost'])){
                $query = '(('.$query.') AND '.$params['hasFieldBoost'].':*^2) OR '.$query;
            }
            if (isset($params['boost'])){
                $query = $params['boost'].$query;
            }
            $q->setQuery($query);
        };

        if (!empty($params['query'])) {
            if (is_string($params['query'])) {
                $setQuery($query = $params['query']);
            } else {
                $conditions = array();
                foreach ($params['query'] as $condition) {
                    if (is_array($condition)) {
                        $qparts = array();
                        foreach ($condition as $key => $qpart) {
                            if (is_array($qpart)) {
                                $where = '(' . implode(' OR ', array_map('SolrUtils::escapeQueryChars', $qpart)) . ')';
                            } else {
                                $where = \SolrUtils::escapeQueryChars($qpart);
                            }
                            $qparts[] = str_replace('?', $where, $key);
                        }
                        $conditions[] = '(' . implode(' ', $qparts) . ')';
                    } else {
                        $conditions[] = $condition;
                    }
                }
                $query = count($conditions) == 1 ? $conditions[0] : implode(' AND ', $conditions);
                $setQuery($query);
            }
        }else{
            $setQuery($query);
        }

        if(!$only_rows_count) {
            if (isset($params['order'])) {
                if (!is_array($params['order'])) {
                    $order = preg_split('/\s/', trim($params['order']), 2);
                    $q->addSortField($order[0], strtolower($order[1]) == 'desc' ? \SolrQuery::ORDER_DESC : \SolrQuery::ORDER_ASC);

                    $query .= ', order: ' . $order[0];
                } elseif (is_array($params['order'])) {
                    foreach ($params['order'] as $order) {
                        $order = preg_split('/\s/', trim($order), 2);
                        $q->addSortField($order[0], strtolower($order[1]) == 'desc' ? \SolrQuery::ORDER_DESC : \SolrQuery::ORDER_ASC);

                        $query .= ', order: ' . $order[0];
                    }
                }
            }
            $time_start = microtime(true);

            $query_response = $this->solr->query($q);
            $query_response->setParseMode(\SolrQueryResponse::PARSE_SOLR_DOC);

            $this->logQuery($query, microtime(true) - $time_start);

            return $this->_processSolrResponse($query_response->getResponse());
        }

        // возврат только количества записей из запроса
        $query_response = $this->solr->query($q);
        return $query_response
            ->getResponse()
            ->response->numFound;
    }


    protected function _getByID($id) {
        $rowClass = $this->itemClass;
        $isMultiQuery = is_array($id) ? true : false;
        $ids = $isMultiQuery ? array_map('strval',array_filter($id) ) : [(string)$id];

        $time_start =  microtime(true);
        try{
            $q = [];
            foreach($ids as $id){
                $q[] = $this->getIdField().':"' . \SolrUtils::escapeQueryChars($id).'"';
            }
            $qobj = new \SolrQuery(implode(' OR ', $q));
            $response = $this->solr->query($qobj);
            $response->setParseMode(\SolrQueryResponse::PARSE_SOLR_DOC);
        }catch(Exception $e){
            throw new Exception("Solr failed to fetch items - ". implode(',', $ids).'. '.$e->getMessage());
        }
        $this->logQuery($this->getIdField(). ':' .implode(',', $ids), microtime(true) - $time_start);

        $rows = [];
        if($response->success() && !empty($response->getResponse()->response->docs) ) {
            $rows = $this->_processSolrResponse($response->getResponse());
        }
        if (!$isMultiQuery){
            return $rows ? array_shift($rows) : null;
        }

        $reorder = [];
        foreach($ids as $one){
            if (isset($rows[$one])){
                $reorder[$one] = $rows[$one];
            }
        }

        return $reorder;
    }

    protected function _processSolrResponse($solrResponse) {

        $result = array();

        $this->lastResponseTime = $solrResponse->responseHeader->QTime;
        $this->lastResponseNumFound = $solrResponse->response->numFound;
        $this->lastResponseStatus = $solrResponse->responseHeader->status;

        $rowClass = $this->itemClass;

        $ids = [];
        foreach ($solrResponse->response->docs as $doc) {
            $element = $this->getServiceManager()->create($rowClass, [$doc, $this, ['queryMetadata' => ['count' => $solrResponse->response->numFound, 'query' => $solrResponse->responseHeader->params->q]]]);
            $ids[] = $id = $doc->{$this->_idKey};
            $result[$id] = $element;
        }
        $this->_saveRealFetchedItemsToCache($result, $ids);

        return $result;
    }

    protected function _saveErrorLog($response){
        throw new Exception(sprintf("Digested Response: \"%s\";\nHttp status:\"%d\";\n
                    Http Status Message:\"%s\" Raw Request: \"%s\";\nRequest Url: \"%s\";",

            $response->getDigestedResponse(),
            $response->getHttpStatus(),
            $response->getHttpStatusMessage(),
            $response->getRawRequest(),
            $response->getRequestUrl()
        ));
    }

    protected $_hasUncommited = false;

	public function insertRow(array $params = []) {

		if(empty($params)) {
            return false;
        }

        try {

            $insertNewDocument = new \SolrInputDocument;

            foreach ($params as $fieldName => $fieldValue) {
                if (is_bool($fieldValue)){

                    if(!$insertNewDocument->addField($fieldName, $fieldValue ? 'true' : 'false')) {
                        throw new \Exception(sprintf('Field: "%s" is not set value: "%s"', $fieldName, $fieldValue ? 'true' : 'false'));
                    }
                }else if(!is_array($fieldValue)) {
                    if(!$insertNewDocument->addField($fieldName, $fieldValue)) {
                        throw new \Exception(sprintf('Field: "%s" is not set value: "%s"', $fieldName, $fieldValue));
                    }

                }else{
                    foreach($fieldValue as $elementValue) {
                        if(is_scalar($elementValue)) {
                            if(!$insertNewDocument->addField($fieldName, $elementValue)) {
                                throw new \Exception(sprintf('Field: "%s" is not set value: "%s"', $fieldName, $fieldValue));
                            }
                        }
                        else {
                            throw new Exception(sprintf("Field: \"%s\" is not storage scalar value must be string float or integer value", $fieldName));
                        }
                    }
                }
            }

            $response = $this->solr->addDocument($insertNewDocument);
            $this->_hasUncommited = true;

            if (isset($response) && !$response->success()) {
                $this->is_error = true;
                $this->_saveErrorLog($response);
            }
            elseif(isset($response) && !$response->success()) {
                $this->is_error = false;
            }

            return true;
        }
        catch(SolrServerException $e) {
            if(isset(self::getConfig()['is_debug']) && self::getConfig()['is_debug']) {
                throw $e;
            }
            trigger_error($e->getMessage(), E_USER_NOTICE);
        }

        return false;
	}

    public function isTransaction(){
        return $this->_hasUncommited;
    }

    public function beginTransaction() {
        return $this;
    }

	public function commit() {
	    if(!$this->is_error) {
            $response = $this->solr->commit();
            if (!$response->success()) {
                $this->rollback();
                $this->_saveErrorLog($response);
            } else {
                $this->_hasUncommited = false;
            }
        }
        return $this;
    }

    public function rollback() {
        $response = $this->solr->rollback();
        if(!$response->success()) {
            $this->_hasUncommited = false;
            $this->_saveErrorLog($response);
        }
        return $this;
    }


    public function updateRow($id = null, array $params = []) {
        if (!isset($params[$this->_idKey]) && $id) {
            $params[$this->_idKey] = $id;
            return boolval($this->insertRow($params));
        }

        return false;
    }

    public function insertObject(Abstract_Item $object) {
        return $this->insertRow($object->toArray());
    }

    public function save(Abstract_Item $object) {
        if (!($object instanceof $this->itemClass) ){
            throw new Exception(get_class($this)." can only save a ".$this->itemClass." instances");
        }

        $dataToSave = $object->toArray();
        $cleanData = $object->getCleanData();
        $idField = $this->getIdField();

        if ( isset($cleanData[$idField]) ){
            $dataToSave[$idField] = $cleanData[$idField];
        }
        unset($dataToSave['_version_']);

        $this->_saveRealFetchedItemsToCache($object, [$object->getId()]);
        return $this->insertRow($dataToSave);
    }

    public function deleteObject(Abstract_Item $object) {

        try {
            $response = $this->solr->deleteById($object->{$this->getIdField()});

        }
        catch(Exception $e) {
            throw $e;
        }

        if($result = $response->success()) {
            $this->commit();
            return $result;
        }

        return false;
    }

    public function getSolrClient() {
        return $this->solr;
    }

    public function remove(Abstract_Item $item)
    {
        return $this->deleteObject($item);
    }

    public function deleteByParams($params = []) {

        if(!empty($params) && is_array($params)) {

            $request_str = "";
            $maxk = count($params);
            $pos = 0;
            $count = $this->findAll($params, true);

            foreach($params as $key => $value) {
                $request_str .= $key.': "'. \SolrUtils::escapeQueryChars($value). (($pos < $maxk && $maxk != 1) ? " and ": "")."\"" ;
                $pos++;
            }

            $query_response = $this->getSolrClient()->deleteByQuery($request_str);
            $response =  $query_response
                ->getResponse();
            $this->commit();
            return $count;
        }

        return 0;
    }



}
