<?php

/**
 * Microservice
 *
 * @package    System
 * @version    1.0
 */
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

/**
 * Класс для выборки через Zend Db
 */
abstract class Db_Zend_Repository extends Abstract_Repository
{
    protected $_table;
    protected $table;
    protected $adapter;
    protected $sql;

    protected static $_connections = [];
    protected $_autoTransactionEnabled = true;

    const FETCH_ITEMS = 1;
    const FETCH_ROWS = 2;
    const FETCH_COL = 3;
    const FETCH_ONE = 4;
    const FETCH_ROW = 5;
    const FETCH_ITEM = 6;

    public function __construct($params = null)
    {
        if (isset($params['table'])) {
            $this->setTable($params['table']);
        }

        if (isset($params['field_id'])) {
            $this->setIdField($params['field_id']);
        }

        if (isset($params['connect'])) {
            ksort($params['connect']);
            $this->_connectConfig = $params['connect'];

            $staticKey = implode('-', $this->_connectConfig);
            $this->adapter = isset(self::$_connections[$staticKey]) ? self::$_connections[$staticKey] : (self::$_connections[$staticKey] = new \Zend\Db\Adapter\Adapter($this->_connectConfig));

            if (empty($this->_table)) {
                trigger_error(sprintf("Please set \"table\" in config for repository: %s ", get_class($this)));
            }

            $this->table = new TableGateway(
                $this->getTable(),
                $this->adapter
            );
        } else {
            trigger_error(sprintf("Please set \"connect\" in config for repository: %s ", get_class($this)));
        }

        $this->sql = new Sql(
            $this->adapter
        );

        parent::__construct($params);
    }

    public function isConnected()
    {
        return $this->adapter->getDriver()->getConnection()->isConnected();
    }

    public function reconnect()
    {
        return $this->adapter->getDriver()->getConnection()->disconnect()->connect();
    }

    public function setWaitTimeout($timeout = 600)
    {
        $this->adapter->getDriver()->getConnection()->execute(sprintf('SET wait_timeout=%d;', $timeout));
        $this->adapter->getDriver()->getConnection()->execute(sprintf('SET innodb_lock_wait_timeout=%d;', $timeout));
    }

    public function select($from = null)
    {
        return $this->sql->select()->from($from ? $from : $this->getTable());
    }

    public function fetch($select, $fetchWhat = self::FETCH_ITEMS)
    {
        $time_start = microtime(true);

        // для поддержки параметра rawsql
        if (!($select instanceof \Zend\Db\Adapter\Driver\Pdo\Result)) {

            if (in_array($fetchWhat, [self::FETCH_ONE, self::FETCH_ROW, self::FETCH_ITEM])) {
                $select->limit(1);
            }

            $result = $this->sql->prepareStatementForSqlObject($select)->execute();

            $this->logQuery($select->getSqlString(
                $this->adapter->getPlatform()
            ), microtime(true) - $time_start);

        } else {
            $result = $select;
        }

        if ($fetchWhat == self::FETCH_ROWS) {
            return iterator_to_array($result);

        } else if ($fetchWhat == self::FETCH_ROW) {
            return $result->current() ? $result->current() : null;

        } else if ($fetchWhat == self::FETCH_ITEMS || $fetchWhat == self::FETCH_ITEM) {

            $items = [];
            $ids = [];
            $itemClass = $this->itemClass;

            foreach ($result as $row) {
                $item = $this->getServiceManager()->create($itemClass, [$row,
                    $this,
                    $this->getItemSettings()]);

                $ids[] = $id = $item->{$this->getIdField()};

                if ($fetchWhat == self::FETCH_ITEM) {
                    $this->_saveRealFetchedItemsToCache($item, $ids);
                    return $item;
                }
                $items[$id] = $item;
            }

            if ($fetchWhat == self::FETCH_ITEMS) {
                $this->_saveRealFetchedItemsToCache($items, $ids);
            }

            return $fetchWhat == self::FETCH_ITEM ? null : $items;

        } else if ($fetchWhat == self::FETCH_COL) {

            return array_map(function ($row) {
                return array_shift($row);
            }, iterator_to_array($result));

        } else if ($fetchWhat == self::FETCH_ONE) {

            if (!$result->current()) {
                return null;
            }

            $firstRow = $result->current();
            return $firstRow ? array_shift($firstRow) : null;
        }

        throw new Exception("Wrong fetch mode");
    }

    public function fetchItems($select)
    {
        return $this->fetch($select, self::FETCH_ITEMS);
    }

    public function fetchRows($select)
    {
        return $this->fetch($select, self::FETCH_ROWS);
    }

    public function fetchCol($select)
    {
        return $this->fetch($select, self::FETCH_COL);
    }

    public function fetchOne($select)
    {
        return $this->fetch($select, self::FETCH_ONE);
    }

    public function fetchRow($select)
    {
        return $this->fetch($select, self::FETCH_ROW);
    }

    public function fetchItem($select)
    {
        return $this->fetch($select, self::FETCH_ITEM);
    }

    /**
     * @return Abstract_Item[]
     * @description основной метод выборки и трансформации в объекты
     *
     *  string  @$params['class']   - имя класса элемента
     *  array   @$params['table']   - таблица
     *  array   @$params['field']  - заполняемые свойства
     *  integer @$params['offset']  - от
     *  integer @$params['limit']   - до
     *  array   @$params['where'] - условия выборки
     *  array   @$params['order']    - сортировки полей
     *  array   @$params['like']   - похожести
     *  array   @$params['join']   - связи с другими таблицами
     * */
    protected function _buildSelectByConfig($params = [])
    {
        $sql = $this->sql;

        $select = $sql->select();
        $select->from(isset($params['table']) ? $params['table'] : $this->getTable());

        if (!empty($params['where'])) {
            if (!empty($params['like'])) {
                $select->where($params['where'])
                    ->like($params['like']);
            } else {
                $select->where($params['where']);
            }
        }

        if (!empty($params['like'])) {
            if (empty($params['where'])) {

                $where = new \Zend\Db\Sql\Where();

                if (is_array($params['like'])) {
                    foreach ($params['like'] as $field => $value) {
                        $where->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like($field, $value)
                        );
                        $select->where($where);
                    }
                }
            }
        }

        if (!empty($params['in']) && is_array($params['in'])) {
            foreach ($params['in'] as $col => $in) {
                $select->where->in($col, $in);
            }
        }

        if (!empty($params['group']))
            $select->group($params['group']);

        if (!empty($params['field']))
            $select->columns($params['field']);

        if (!empty($params['count']))
            $select->columns(['count' => new \Zend\Db\Sql\Expression('COUNT(*)')]);

        if (!empty($params['offset']))
            $select->offset($params['offset']);

        if (!empty($params['limit']))
            $select->limit($params['limit']);

        if (!empty($params['order']))
            $select->order($params['order']);

        if (!empty($params['join'])) {
            foreach ($params['join'] as $join) {
                if (!empty($join[0]) && is_array($join[0]) && !empty($join[1]) && is_string($join[1])) {

                    if (!empty($join[2])) {
                        $select->join($join[0], $join[1], $join[2]);
                    } else {

                        $select->join($join[0], $join[1]);
                    }
                }
            }
        }

        return $select;
    }

    public function findAll($params = [], $only_rows_count = false)
    {
        $select = $this->sql->select();

        if ($only_rows_count) {
            $params['count'] = true;
        }

        if (!isset($params['rawsql'])) {
            $select = $this->_buildSelectByConfig($params);
        } else {
            // получение записей по переданному SQL и параметры
            if (is_string($params['rawsql'])) {
                if (!isset($params['values'])) {
                    $select = $this->getAdapter()
                        ->query($params['rawsql'])
                        ->execute();
                } else {
                    $select = $this->getAdapter()
                        ->query($params['rawsql'])
                        ->execute($params['values']);
                }
            }
        }

        if (!$only_rows_count) {
            // на случай, если нам нужно вернуть массив вместо объектов
            if (isset($params['format']) && $params['format'] == 'array') {
                $items = $this->fetchRows($select);
            } else {
                $items = $this->fetchItems($select);
            }

            return $items;
        } else {
            $items = $this->fetchOne($select);
            return isset($items['count']) ? $items['count'] : intval($items);
        }
    }

    /**
     *  метод получения записи по идентификатору(ам)
     *
     * @param integer | array $id
     * @return Abstract_Item
     */
    protected function _getByID($id)
    {
        $result = $this->fetch(
            $this
                ->select()
                ->where([$this->getIdField() => $id]),
            is_array($id) ? self::FETCH_ITEMS : self::FETCH_ITEM
        );

        if (!is_array($id)) return $result;

        $reorder = [];
        foreach ($id as $one) {
            if (isset($result[$one])) {
                $reorder[$one] = $result[$one];
            }
        }

        return $reorder;
    }

    /**
     * Метод установки текущей таблицы
     *
     * @param string
     * @return none
     */
    public function setTable($table)
    {
        $this->_table = $table;
    }

    /**
     * Метод получения текущей таблицы
     *
     * @param none
     * @return string
     */
    public function getTable()
    {
        return $this->_table;
    }

    public function autoTransaction($enabled = null)
    {
        if ($enabled !== null) {
            $this->_autoTransactionEnabled = $enabled;
            return $this;
        }
        return $this->_autoTransactionEnabled;
    }

    /**
     *  Метод вставки новой записи
     *
     * @param array
     * @return integer
     **/
    public function insertRow(array $params)
    {

        if ($this->_autoTransactionEnabled && !$this->isTransaction()) {
            $this->beginTransaction();
        }

        $this->table->insert($params);
        return $this->table->getLastInsertValue();
    }

    /**
     * Метод обновления записи
     *
     * @param integer
     * @param array
     * @return bool
     **/
    public function updateRow($id, array $params)
    {
        return $this->table->update(
            $params, [
                $this->getIdField() => $id
            ]
        );
    }

    /**
     * Метод обновления объекта в таблице
     *
     * @param Abstract_Item
     * @return bool
     **/
    public function save(Abstract_Item $object)
    {
        if (!($object instanceof $this->itemClass)) {
            throw new Exception(get_class($this) . " can only save a " . $this->itemClass . " instances");
        }

        $idField = $this->getIdField();
        $cleanData = $object->getCleanData();

        if (!$cleanData) {
            $newId = $this->insertRow($object->toArray(false));
            $object->{$idField} = $newId;
        } else {
            if (!$idField || empty($cleanData[$idField])) {
                trigger_error(sprintf("Object is not have IDs field \"%s\"", $this->getIdField()));
                return false;
            }

            //получение данных без дополнительных параметров для Json ответа
            $data = $object->getModifiedData();
            // ! идентификацию изменить нельзя в процессе работы с объектом !
            unset($data[$idField]);

            $this->table->update($data, [$idField => $cleanData[$idField]]);
        }
        $this->_saveRealFetchedItemsToCache($object, [$object->getId()]);

        return true;
    }

    public function isTransaction()
    {
        if ($this->getAdapter()) {
            return $this->adapter->getDriver()->getConnection()->inTransaction();
        }

        return false;
    }

    public function beginTransaction()
    {
        if (!$this->isTransaction()) {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }

        return $this;
    }

    public function commit()
    {
        if ($this->isTransaction()) {
            $this->adapter->getDriver()->getConnection()->commit();
        }

        return $this;
    }

    public function rollback()
    {
        if ($this->isTransaction()) {
            $this->adapter->getDriver()->getConnection()->rollback();
        }

        return $this;
    }

    public function deleteBy(array $expressions)
    {
        $delcount = 0;

        foreach ($expressions as $expression) {
            if ($this->table->delete($expression)) {
                $delcount++;
            }
        }

        return $delcount;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }

    public function remove(Abstract_Item $item)
    {
        $idField = $this->getIdField();

        $delete = $this->sql->delete();
        $delete->from($this->getTable());

        $delete->where([
            $this->getIdField() . '=' . $item->getId()
        ]);

        if ($result = $this->sql->prepareStatementForSqlObject($delete)->execute()) {
            $this->commit();
            return $result;
        }

        return false;
    }

    public function deleteByParams($params = []) {

        if(empty($params) || !is_array($params)) {
            return false;
        }

        $delete = $this->sql->delete();
        $delete->from($this->getTable());
        $delete->where($params);

        if ($result = $this->sql->prepareStatementForSqlObject($delete)->execute()) {
            $this->commit();
            return $result;
        }

        return false;
    }
}


