<?php
class Html_Model extends Abstract_ViewModel {
    protected $template;
    protected $result;

    public function setTemplate($template) {
        $this->template = $template;
        return $this;
    }

    public function __toString()
    {
        if (!empty($this->encoding)) {
            self::sendHeader(
                sprintf('Content-Type: text/html; charset=%s', $this->encoding)
            );
        } else {
            self::sendHeader('Content-Type: text/html');
        }

        $contents = ob_get_contents();
        if (trim($contents)) {
            $this->addError($contents);
        }

        $this->workStateTime();
        ob_end_clean();

        $html = "";
        $data = $this->getProxyObject();

        if (is_array($data) && extract($data)) {
            if (file_exists($this->template)) {
                ob_start();
                include $this->template;
                $html = ob_get_contents();
                ob_end_clean();
            }
        }

        return $html;
    }
}