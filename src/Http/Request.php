<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс управления передаваемыми данными
 */
class Http_Request extends Abstract_Configuration {
    protected $_wholeParams = [];
    protected $_filteredParams = [];
    protected $_isCall = false;

    protected $urlRequest = [];
    protected $getRequest = [];
    protected $postRequest = [];
    protected $cookieRequest = [];
    protected $class = null;
    protected $method = null;
    protected $urlparams = null;
    protected $requestFilter = null;

    public function setUrlParams($urlparams) {
        $this->urlparams = $urlparams;
    }

    public function createRequest($class, $method) {
        $this->class = $class;
        $this->method = $method;
    }

    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function isCall()
    {
        return $this->_isCall;
    }

    /**
     * Получение настроек параметров для вызываемого метода класса.
     *
     * @param null
     * @return array
     */
    public function getRequestFilterFromConfig() {
        $cfg = self::getConfig();

        if (empty($this->class) || empty($this->method)) {
            throw new Exception((empty($this->class) ? 'Method: ' . __METHOD__ . 'Пустой класс' : 'Пустой метод'));
        }

        if (empty($cfg['request_filter']) || !is_array($cfg['request_filter']) || empty($cfg['request_filter'][$this->class]) || !is_array($cfg['request_filter'][$this->class])) {
            return Null_Object::create(__METHOD__);
        }

        // Если indexAction и нет профелированных параметров то возвращаем пустой массив.
        if ($this->method == 'indexAction' && empty($cfg['request_filter'][$this->class][$this->method])) {
            return [];
        }

        if (empty($cfg['request_filter'][$this->class][$this->method]) || !is_array($cfg['request_filter'][$this->class][$this->method])) {
            return Null_Object::create(__METHOD__);
        }

        return $cfg['request_filter'][$this->class][$this->method] ? $cfg['request_filter'][$this->class][$this->method] : Null_Object::create(__METHOD__);
    }

    /**
     * Установка карты передаваемых параметров для вызова методов
     *
     * @param array
     * @return none
     */
    public function setRequestFilter($requestFilter) {
        $this->requestFilter = $requestFilter;
        foreach($this->requestFilter as &$one){
            if (is_array($one) && isset($one['from'])){
                $one['from'] = mb_strtoupper($one['from']);
            }
        }
    }

    /**
     * Проверка нужно ли возвращать весь Request
     *
     * @param null
     * @return array
     */
    public function isNeedReturnRequest() {

        if(isset($this->requestFilter['__isRequest']) && $this->requestFilter['__isRequest']) {
            return true;
        }
	    return false;
    }

    /**
     * Обработка передаваемых параметров
     *
     * @param mixed $value
     * @param string $name
     * @param array $settings
     * @return array
     */
    protected function filtrateParam($value, $name, $settings) {

        $result = [];

        if ((isset($value) && ($value !== 0)) || !empty($value)) {
            if ( isset($settings["from"]) ){
                if (in_array(strtoupper($settings["from"]), ['URL', 'GET'])) {
                    if(is_string($value)) {
                        $value = urldecode($value);
                    }
                }
            }
            $prefilter = function($type, $value){
                if ($type) {
                    if ($type == 'json'){
                        try {
                            if(is_object($value)){
                                return $value;
                            }else if (is_array($value)){
                                return $value;
                            }else if ( in_array(substr((string)$value, 0, 1), ['{','[','"'])  ){
                                return json_decode((string)$value, true);
                            }else if (preg_match('@^[0-9,\s]+$@', (string)$value) ) {
                                return array_map('intval', explode(',', (string)$value));
                            }else{
                                return [];
                            }
                        }catch(Exception $e){
                            return [];
                        }
                    }else{
                        $result = null;
                        eval('$result = (' . $type . ')@$value;');
                        return $result;
                    }
                } else {
                    return (string)$value;
                }
            };

            // проверка регулярным выражением
            if (isset($settings['regexp'])) {

                if (preg_match($settings['regexp'], (string)$value, $value_match)) {
                    $value = isset($value_match[1]) ? $value_match[1] : $value;

                    $result[$name] = $prefilter(isset($settings['type']) ? $settings['type'] : null, $value);
                } else {
                    if(empty($value) && empty($settings['default'])) {
                        $result[$name] = $value;
                    } else {
                        throw new Exception("Regexp: ".$settings['error']);
                    }
                }
            } else {
                $result[$name] = $prefilter(isset($settings['type']) ? $settings['type'] : null, $value);
            }
        }else{
            $result[$name] = isset($settings['default']) ? $settings['default'] : null;
            if ( isset($settings['error']) && !isset($settings['default']) ){
                echo json_encode(['error' => $settings['error']]);
                throw new Exception($settings['error']);
            }
        }
        return $result;
    }

    /**
     * Метод проверки и обработки передаваемых параметров
     *
     * @param array|'URL'|'GET'|'POST'|'COOKIE' $typeOrParams , в случае если передан массив - параметры парсятся из него, иначе из глобального массива, соответствующего одной из строк
     * @return array
     */
    public function checkParams($typeOrParams) {

        $result = [];
        $param = [];
        
        if (empty($this->requestFilter)) {
            $this->requestFilter = $this->getRequestFilterFromConfig();
        }
        if (is_array($typeOrParams)){
            $isGlobalArrayCheck = false;
            $sendParams = $typeOrParams;
            $this->_isCall = true;
        }else{
            $isGlobalArrayCheck = true;
            $type = $typeOrParams;
            $sendParams = $this->getSendData($type);
        }

        if (!empty($this->requestFilter) || is_array($this->requestFilter)) {

            // фильтрация настроек
            foreach ($this->requestFilter as $param_name => $settings) {
                if(mb_substr($param_name, 0, 2) == '__') {
                    continue;
                }
                if ($isGlobalArrayCheck && (isset($settings['from']) && $settings['from'] == $type) || !$isGlobalArrayCheck){
                    $result = array_merge($result, $this->filtrateParam(
                        isset($sendParams[$param_name]) ? $sendParams[$param_name] : null, $param_name, $settings
                    ) );
                    $this->_wholeParams[$param_name] = $result[$param_name];
                }
            }
        }

        foreach($sendParams as $key=>$value){
            if (!isset($this->_wholeParams[$key])) {
                $this->_wholeParams[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Получение обработаных данных + данных, для которых не установлен фильтр - по опции.
     *
     * @param bool $onlyFilterExists
     * @return array
     */

    public function getParams($onlyFilterExists = false) {
        if ($this->_filteredParams){
            return $onlyFilterExists ? $this->_filteredParams  : $this->_wholeParams;
        }
        foreach($this->requestFilter as $paramName => $paramConfig ){
            if( mb_substr($paramName, 0, 2) == '__') {
                continue;
            }
            $from = $this->getFiltratedSendData($paramConfig['from']);

            if (isset($this->_wholeParams[$paramName]) && !isset($from[$paramName]) ){
                $this->_filteredParams[$paramName] = $this->_wholeParams[$paramName];
            }else{
                $this->_wholeParams[$paramName] = $this->_filteredParams[$paramName]  = isset($from[$paramName]) ? $from[$paramName] : null;
            }
        }

        return $onlyFilterExists ? $this->_filteredParams : $this->_wholeParams;
    }

    /**
     * Последовательный запуск обработки параметров
     *
     * @param null
     * @return null
     */
    public function filtrateParams() {

        // обработка параметров переданных через урл
        $this->urlRequest = $this->checkParams('URL');

        // обработка параметров переданных через post
        $this->postRequest = $this->checkParams('POST');

        // обработка параметров переданных через get
        $this->getRequest = $this->checkParams('GET');


        $this->cookieRequest = $this->checkParams('COOKIE');
    }

    /**
     * Метод возвращает не-нормализованый массив переданных данных
     *
     * @param string $type (URL | GET | POST | COOKIE)
     * @return array | null
     */
    private function getSendData($type) {
        if (isset($type)) {
            switch ($type) {
                case 'URL':
                    return isset($this->urlparams) ? $this->urlparams : [];
                    break;

                case 'COOKIE':
                    return $_COOKIE;
                    break;

                case 'POST':
                    return $_POST;
                    break;

                case 'GET':
                    return $_GET;
                    break;
            }
        }
    }

    /**
     * Метод возвращает нормализованый Массив переданных данных
     *
     * @param string $type (URL | GET | POST | COOKIE)
     * @return array | null
     */
    private function getFiltratedSendData($type) {
        if (isset($type)) {
            switch ($type) {
                case 'URL':
                    return $this->urlRequest;
                    break;

                case 'COOKIE':
                    return $this->cookieRequest;
                    break;

                case 'POST':
                    return $this->postRequest;
                    break;

                case 'GET':
                    return $this->getRequest;
                    break;
            }
        }
    }

    /**
     * Получение передаваемых файлов
     *
     * @param null
     * @return array
     */
    public function getFilesData() {
        return $_FILES;
    }

    /**
     * Получение обработаных данных через запрос POST
     *
     * @param null
     * @return array
     */
    public function getFiltratedPostData() {
        return $this->postRequest;
    }

    /**
     * Получение обработаных данных через запрос GET
     *
     * @param null
     * @return array
     */
    public function getFiltratedGetData() {
        return $this->getRequest;
    }

    /**
     * Получение обработаных данных через запрос URL
     *
     * @param null
     * @return array
     */
    public function getFiltratedUrlData() {
        return $this->urlRequest;
    }

    /**
     * Получение обработаных данных через запрос COOKIE
     *
     * @param null
     * @return array
     */
    public function getFiltratedCookieData() {
        return $this->cookieRequest;
    }


    public function setPostData($postData) {
        $this->postRequest = $postData;
    }
    
}
