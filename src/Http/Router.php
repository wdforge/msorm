<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс управления роутингом приложения
 */
class Http_Router extends Abstract_Configuration {

    protected $routeInfo = [];

    /**
     * Метод для парсинга URL запроса
     *
     * @param none
     * @return array
     */
    public function getRoute($requestUri = null) {

        $cfg = self::getConfig();

        $result = [];
        $url = (!$requestUri) ? (isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $_SERVER['REQUEST_URI']) : $requestUri;

        // очищаем url от _GET запроса
        $urlarr = explode('?', $url);
        $url = $urlarr[0];

        $host_level = 0;


        if (!isset($cfg['service_is_host']) ||
            !$cfg['service_is_host'] ||
            !isset($cfg['primary_controller'][$cfg['servicename']])) {

            $host_level = 1;
        }

        if (isset($url[strlen($url) - 1]) && $url[strlen($url) - 1] == '/') {
            $url = substr($url, 0, -1);
        }

        if (isset($url[strlen($url) - 1]) && $url[strlen($url) - 1] == '/') {
            $url = substr($url, 0, -1);
        }

        if (isset($url[0]) && $url[0] == '/') {
            $url = substr($url, 1);
        }

        $chunked = explode('/', $url);

        $minus_level = 0;
        $primary_controller = '';

        if (isset($cfg['primary_controller'][$cfg['servicename']])) {
            $primary_controller = str_replace('_Controller', '', $cfg['primary_controller'][$cfg['servicename']]);
            $minus_level = 1;
        }
        else {
            $minus_level = 1;
        }

        if (count($chunked) == 1) {
            if (empty($chunked[(0 + $host_level)])) {

                $result = [
                    'controller' => isset($cfg['primary_controller'][$cfg['servicename']]) ? $primary_controller : $chunked[0],
                    'action' => 'index',
                    'params' => array(),
                ];

                $this->routeInfo = $result;
                return $this->routeInfo;
            }
        }

        if (count($chunked) == ((2 + $host_level) - $minus_level)) {
            $result = [
                'controller' => isset($cfg['primary_controller'][$cfg['servicename']]) ? $primary_controller : $chunked[0],
                'action' => $chunked[1 - $minus_level + $host_level],
                'params' => array(),
            ];
        }

        if(!isset($cfg['primary_controller'][$cfg['servicename']]) && $host_level) {
            if (count($chunked) > ((2 + $host_level) - $minus_level)) {
                $result = [
                    'service' => $chunked[0],
                    'controller' => $chunked[0 + $host_level],
                    'action' => $chunked[2 - $minus_level + $host_level],
                    'params' => array_slice($chunked, 3 - $minus_level + $host_level),
                ];
            }
            elseif(count($chunked) > ((1 + $host_level) - $minus_level)) {
                $result = [
                    'service' => $chunked[0],
                    'controller' =>  $chunked[0 + $host_level],
                    'action' => isset($chunked[1 + $host_level]) ? $chunked[1 + $host_level]: 'index',
                    'params' => array_slice($chunked, 2 + $host_level),
                ];
            }
        }
        else {
            if (count($chunked) > ((2 + $host_level) - $minus_level)) {
                $result = [
                    'controller' => isset($cfg['primary_controller'][$cfg['servicename']]) ? $primary_controller : $chunked[0 + $host_level],
                    'action' => $chunked[1 - $minus_level + $host_level],
                    'params' => array_slice($chunked, 2 - $minus_level + $host_level),
                ];
            }
        }
        if (isset($result['action'])){
            $result['action'] = mb_strtolower(mb_substr($result['action'], 0, 1) ) . mb_substr($result['action'], 1);
        }

        $this->routeInfo = $result;
        return $this->routeInfo;
    }

    /**
     * Метод для определения имени контроллера исходя из короткого имени
     *
     * @param string $shortName
     * @return string
     */
    public static function getControllerName($shortName) {

        if (!empty($shortName)) {

            $class_name = ucfirst($shortName) . '_Controller';

            if(strpos($class_name, '-') !== false) {

                $res = explode('-', $class_name);
                $class_name = '';

                foreach($res as $sre) {
                    $class_name .= ucfirst($sre);
                }
            }

            return $class_name;
        }

        return false;
    }

    /**
     * Метод для определения имени метода исходя из короткого имени
     *
     * @param string $shortName
     * @return string
     */
    public static function getMethodName($shortName) {

        if (!empty($shortName)) {
            $method_name = $shortName . 'Action';

            return $method_name;
        }

        return false;
    }

    /**
     *
     * @param none
     * @return array
     */
    public function getRouteInfo() {
        return $this->routeInfo;
    }

    /**
     *
     * @param array
     * @return none
     */
    public function setRouteInfo(array $routeInfo) {
        $this->routeInfo = $routeInfo;
    }

    /**
     *
     * @param array
     * @return none
     */
    public function setMapData($filterArray) {

        $n = 0;

	    if($filterArray)
        foreach ($filterArray as $key => $filter) {

            // параметр фильтрации их пропускаем.
            if(mb_substr($key, 0, 2) == '__' ) {
                continue;
            }

            if (isset($filter['from']) && $filter['from']!='URL'){
                continue;
            }

            $this->routeInfo['params'][$key] = isset($this->routeInfo['params'][$n]) ? $this->routeInfo['params'][$n] :  null;
            unset($this->routeInfo['params'][$n]);
            $n++;
        }

        $this->routeInfo['params'] = array_merge($this->routeInfo['params'], []);
        return $this->routeInfo['params'];
    }

}
