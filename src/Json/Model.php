<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс видовой модели для работы с json данными
 */
class Json_Model extends Abstract_ViewModel {

    public function __toString()
    {
        if (!empty($this->encoding)) {
            self::sendHeader(
                sprintf('Content-Type: application/json; charset=%s', $this->encoding)
            );
        } else {
            self::sendHeader('Content-Type: application/json');
        }

        $contents = ob_get_contents();
        $proxyObject = $this->getProxyObject();

        if (trim($contents)) {
            $this->addError($contents);
        }

        $this->workStateTime();
        ob_end_clean();

        if (!empty($this->encoding)) {

            $this->setCharsetProxyObject($this->encoding);

            $json = json_encode(
                $proxyObject, JSON_UNESCAPED_UNICODE
            );
        } else {
            $json = json_encode(
                $proxyObject
            );
        }

        return $json;
    }
}
