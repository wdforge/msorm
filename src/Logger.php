<?php
/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс для логгирования
 */
class Logger extends Abstract_Configuration {

    static public function Log($log) {

        $cfg = self::getConfig();

		if(isset($cfg['servicename']) && isset($cfg['logfile'])) {

	        $logfile = str_replace("|service|", $cfg['servicename'], $cfg['logfile']);

            $dir = dirname($logfile);

            if(!is_dir($dir)) {

                if(!mkdir($dir, 0777, true)) {
					echo sprintf("Не удаётся создать лог фаил: %s.", $dir);
				}
            }

    	    if (!empty($log) && is_string($log) && !empty($logfile) && $file = fopen($logfile, "a")) {
        	    $date_str = date('Y-m-d H:i:s');
            	fwrite($file, "\n" . $date_str . ":  " . $log . "\n");
	            fclose($file);
    	    }
		}
    }
}
