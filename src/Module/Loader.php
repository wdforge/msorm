<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Загрузчик модулей (общих для всех микросервисов)
 */
class Module_Loader extends Abstract_Configuration {

    public function LoadModules($moduleList) {
        if (!empty($moduleList) && isset($moduleList) && is_array($moduleList)) {
            foreach ($moduleList as $moduleName) {
                $this->LoadModule($moduleName);
            }
        }
    }

    public function LoadModule($moduleName) {
        $cfg = self::getConfig();

        $path = $cfg['module_dir'];
        $path = ($path[strlen($path) - 1] == DIRECTORY_SEPARATOR) ? $path : $path . DIRECTORY_SEPARATOR;

        if (!empty($path) && is_dir($path . $moduleName)) {
            $config_path = $path . $moduleName . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php';

            if (file_exists($config_path)) {
                self::loadConfig($config_path);
                $cfg = self::getConfig();

                return true;
            } else {
                throw new Exception('Config ' . $config_path . "\n" . 'From module: ' . $moduleName . ' not found');
            }
        } else {
            throw new Exception('Module path  ' . $path . $moduleName . ' not found');
        }

        return true;
    }

}
