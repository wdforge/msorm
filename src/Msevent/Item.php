<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс событие
 */
class Msevent_Item {

    public $name;
    public $appDir;
    public $appName;
    public $isThis;
    public $result;

}
