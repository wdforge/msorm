<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс observer для управления событиями
 */
class Msevent_Manager extends Abstract_Configuration {

    protected $event;
    protected static $_events = [];
    protected static $_microservicesObserved = [];

    protected static $_instance = null;
    function __construct() {
        if (self::$_instance){
            throw new Exception("Can't construct more instances of Msevent_Manager");
        }
        $this->getAllServicesListEvents();
        self::$_instance = $this;
    }
    
    public static function getInstance()
    {
        return self::$_instance;
    }

    /**
     * Метод осуществляет автозагрузку настроек событий сервисов (приложений)
     *
     * @param none
     * @return array
     */
    protected function getAllServicesListEvents() {

        $cfg = self::getConfig();
        $dir = empty($cfg['services_dir']) ? dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'services' . DIRECTORY_SEPARATOR : $cfg['services_dir'];

        if (is_dir($dir)) {

            $result = [];

            if ($handle = opendir($dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..')
                        continue;
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $file))
                        $result[] = DIRECTORY_SEPARATOR . $file;
                }

                closedir($handle);
            }

            foreach ($result as $serviceDirectory) {

                if ($path = realpath($dir . $serviceDirectory . DIRECTORY_SEPARATOR . 'events.php')) {

                    $cfg_events = include($path);

                    if (!is_array($cfg_events)) {
                        continue;
                    }

                    // чтобы вернуть путь до сервиса
                    $keys = array_keys($cfg_events);

                    if (!empty($keys)) {
                        $key = current($keys);
                        $cfg_events[$key]['serviceDirectory'] = $dir . $serviceDirectory;
                    }

                    if (!empty($cfg_events)) {
                        self::$_events = array_merge_recursive(self::$_events, $cfg_events);
                    }
                }
            }
        } else {

            if (realpath($dir)) {
                throw new Exception('Path: ' . $dir . ' is not directory');
            } else {
                throw new Exception('Path: ' . $dir . ' is not found');
            }
        }

        return self::$_events;
    }

    /**
     * Метод осуществляет запуск обработок события
     *
     * @param Msvent $event
     * @return null
     */
    protected function execEvent(&$event) {

        if (!$event->isThis) {
            // выполнение упрощенного профиля приложения
            if (!isset(self::$_microservicesObserved[$event->appName])) {

                self::loadConfigs([
                    // глобальные конфиги
                    ROOT_SERVER_CONFIG,
                    ROOT_SERVICES_CONFIG,
                    // сервисный конфиг
                    $this->event->appDir . '/config.php',
                ]);

                self::$_microservicesObserved[$event->appName] = new $event->appName();
            }

            self::$_microservicesObserved[$event->appName]->{$event->name}($event);
        }

        if ($event->isThis) {
            if (method_exists($this, $event->name)) {
                $this->{$event->name}($event);
            } else {
                throw new Exception("event not found \"" . get_class($this) . "::" . $event->name . "\"");
            }
        }
    }

    /**
     * Метод осуществляет формирование объекта Msevent_Item и передачу управления на запуск обработок
     *
     * @param Event $event
     * @return null
     */
    public function triggerEvent($eventName) {
        $serviceEvents = self::$_events;

        $this->event = isset($this->event) ? $this->event : new Msevent_Item;

        foreach ($serviceEvents as $serviceName => $eventSelect) {
            if (is_array($eventSelect)) {
                foreach ($eventSelect as $event) {
                    if ($eventName == $event) {

                        $this->event->name = $eventName;
                        $this->event->appDir = $eventSelect['serviceDirectory'];
                        $this->event->appName = $serviceName;
                        $this->event->isThis = ($serviceName == get_class($this) ? 1 : 0);

                        // вызов события
                        $this->execEvent($this->event);
                    }
                }
            }
        }
    }

    /**
     * Метод возвращает последний объект события.
     *
     * @param none
     * @return none
     */
    public function getLastEvent() {
        return $this->event;
    }

    /**
     * Получение экземпляра подключенного сервиса
     *
     * @param $appName string
     * @return Abstract_Application
     */
    public function geObservedService($appName) {
        return isset(self::$_microservicesObserved[$appName])? self::$_microservicesObserved[$appName]: null;
    }

}
