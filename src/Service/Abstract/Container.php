<?php
/**
 * Microservice
 *
 * @package    System/Service/Abstract
 * @version    1.0
 */

/**
 * Класс реализующий базовый функционал контейнера объекта
 */

abstract class Service_Abstract_Container extends Action_Object implements JsonSerializable {

    protected $_instance;

    public function jsonSerialize () {
        return $this->getInstance();
    }

    public function __construct($object)
    {
        // конструирование объекта по пабору параметров
        $this->_instance = $object;
    }

    public function __isset($name)
    {
        return isset($this->getInstance()->{$name});
    }

    public function getInstance() {
        return $this->_instance;
    }

    public static function getCallable() {

        return (function($object, $name, $arguments){
            $result = null;

            if(!is_object($object)) {
                trigger_error('Param object is not Object');
            }
            // вызов метода
            if($object instanceof Action_Controller) {
                if(strlen($name)> 6 && substr($name, strlen($name)-6, 6) == 'Action') {
                    $result = $object->dispatch($name, isset($arguments['params']) ? $arguments['params'] : [], isset($arguments['request']) ? $arguments['request'] : null);
                }
                else {
                    $result = call_user_func_array([$object, $name], $arguments);
                }
            }
            else {
                $result = call_user_func_array([$object, $name], $arguments);
            }

            $hash = hash('md5', get_class($object).$name);
            // при вызове должен срабатывать связанный каллбэк из Service_Manager
            if($callback = self::getStaticServiceManager()
                ->get($hash)) {
                $result = method_exists($object, 'on'.ucfirst($name).'After')? $object->{'on'.ucfirst($name).'After'}($result): (
                (is_object($result) && $result instanceof Abstract_Item) ? $result->toArray(): $result);

                // вызов метода OnMethod($result)
                $callback([
                    'class' => get_class($object),
                    'method' => $name,
                    'result' => $result
                ]);
            }

            return $result;
        });
    }

    public function __call($name, $arguments)
    {
        $callable = self::getCallable();
        return $callable($this->getInstance(), $name, $arguments);
    }

    public function __get($name)
    {
        return $this->getInstance()->{$name};
    }

    public function __set($name, $value)
    {
        $this->getInstance()->{$name} = $value;
    }

    public function __unset($name)
    {
        unset($this->getInstance()->{$name});
    }
}
