<?php
/**
 * Microservice
 *
 * @package    System/Service/Abstract
 * @version    1.0
 */

/**
 * Класс шаблон для фабрики Service_Manager
 */

abstract class Service_Abstract_Factory {
    abstract public function createObject($class, array $params);
}
