<?php

/**
 * Microservice
 * 
 * @package    System
 * @version    1.0
 */

/**
 * Класс для управления зависимостями
 */
class Service_Manager extends Abstract_Configuration {

    protected static $_lazyLoaders = [];
    protected static $_services = [];
    protected static $_repositories = [];

    // добавление репозитория
    public static function init($name, $class, $config) {
        if (class_exists($class)) {
            self::$_services[$name] = new $class(empty($config) ? [] : $config);
            return self::$_services[$name];
        } else {
            Logger::log(__METHOD__ . " class \"" . (string) @$class . "\"not found ");
        }

        return Null_Object::create(__METHOD__);
    }

    // возвращении значения/объекта
    public static function get($name) {
        if (empty(self::$_services[$name]) && isset(self::$_lazyLoaders[$name])){
            $callback = self::$_lazyLoaders[$name];
            self::$_services[$name] = $callback($name);
        }

        return !empty(self::$_services[$name]) ? self::$_services[$name] : Null_Object::create(__METHOD__);
    }

    /**
     *
     * @todo метод создания объектов по настройкам связей интерфейсов с фабриками и контейнерами
     *
     * @param $class string
     * @param $params array
     * @return object
     *
    **/
    public function create($class, $params = []) {

        if(!$class) {
            return null;
        }

        $result = null;
        $cfg = self::getConfig();
        $interfaces = class_implements($class);

        // связь интерфейс => фактори
        if(isset($cfg['service_manager']['factories']) && is_array($cfg['service_manager']['factories'])) {
            foreach ($cfg['service_manager']['factories'] as $class_interface => $class_factory) {
                if (in_array($class_interface, $interfaces)) {
                    $reflection_factory = new ReflectionClass($class_factory);
                    if($reflection_factory->isSubclassOf(Service_Abstract_Factory::class)) {
                        $result = $class_factory::createObject($class, $params);
                    }
                    else {
                        throw new Exception(
                            sprintf('Factory "%s" is not sub class of "%s"', $class_factory, Service_Abstract_Factory::class)
                        );
                    }
                }
            }
        }

        $reflection = new ReflectionClass($class);
        if(!$result) {
            $resource =  $reflection->newInstanceArgs($params);
            $result = $resource;
        }

        // связь интерфейс => контейнер
        if(isset($cfg['service_manager']['containers']) && is_array($cfg['service_manager']['containers'])) {
            foreach ($cfg['service_manager']['containers'] as $class_interface => $class_container) {
                // если у объекта присутствует выбранный интерфейс, помещаем объект в связанный контейнер
                if($reflection->implementsInterface($class_interface)) {
                    $reflection_container = new ReflectionClass($class_container);
                    $result = $reflection_container->newInstance($result);
                }
            }
        }

        return $result;
    }

    // установка значения/объекта
    public function set($name, $value) {
        if (is_null(self::$_services)) {
            self::$_services = [];
        }

        self::$_services[$name] = $value;
    }

    public function setLazy($name, callable  $fabricMethod){
        self::$_lazyLoaders[$name] = $fabricMethod;
    }

    // получение экземпляра репозитория
    /* @return Abstract_Repository */
    public function getRepository($itemClass) {
        static $modules = [];

        $cfg = self::getConfig();
        $repository = false;

        $matchModule = function($itemClass) use($cfg, &$modules) {
            $exploded = explode('Item_', $itemClass);
            $toAnalyse = lcfirst(count($exploded) >= 2 ? $exploded[1] : $itemClass);

            if (!$modules){
                $toCamelStyle = function($inString){
                    return  lcfirst( implode('', array_map('ucfirst', explode('-', $inString)) ));
                };
                $dirs = glob($cfg['services_dir']. '/*' , GLOB_ONLYDIR);
                foreach($dirs as $dir){
                    if(preg_match('@([^/\\\\]+)$@u', $dir, $matches)) {
                        $modules[] = $toCamelStyle($matches[1]);
                    }
                }
            }
            foreach($modules as $module){
               $part2 = explode('_', $toAnalyse)[0];
                if ( mb_strpos($module, $part2 ) !==false || mb_strpos($part2, $module ) !==false ){
                    return $module;
                }
            }
            return false;
        };

        if (empty($cfg['repositories'][$itemClass]['class']) && empty(self::$_repositories[$itemClass]) && ($module = $matchModule($itemClass))){
            Msevent_Manager::getInstance()->triggerEvent('OnUse'.ucfirst($module).'Service');
            $cfg = self::getConfig();
        }

        if (!empty($cfg['repositories'][$itemClass]['class'])) {
                if (empty(self::$_repositories[$itemClass])) {
                    if (!empty($cfg['repositories'][$itemClass]['params'])) {
                        $repository = $this->create($cfg['repositories'][$itemClass]['class'], [$cfg['repositories'][$itemClass]['params']]);
                    } else {
                        $repository = $this->create($cfg['repositories'][$itemClass]['class']);
                    }

                    $repository->setItemClass($itemClass);

                    if(!empty($cfg['repositories'][$itemClass]['params']['element-settings'])) {
                        $repository->setItemSettings($cfg['repositories'][$itemClass]['params']['element-settings']);
                    }
                    self::$_repositories[$itemClass] = self::$_repositories[get_class($repository)] = $repository;
                } else {
                    $repository = self::$_repositories[$itemClass];
                }
        } else {
            trigger_error(sprintf('No set repository for object "%s"', $itemClass));
        }

        return $repository ? $repository : Null_Object::create(__METHOD__);
    }

    public function getLoadedRepositories()
    {
        return self::$_repositories;
    }

    /**
     * @todo получение полей объекта по заданным спискам полей
     *
     * @param $object array | object
     * @param $interface array | string | none
     *
     * @return array
     **/
    public function filter($object, $interface = '') {

        if(is_object($object)) {
            $class = get_class($object);
            if(empty($interface)) {
                $interfaces = class_implements($class);
            } else {
                if(is_array($interface)) {
                    $interfaces = $interface;
                } else {
                    $interfaces = [$interface];
                }
            }
        }
        elseif(is_array($object) && !empty($interface)) {
            $interfaces = [$interface];
        } else {
            return;
        }

        $cfg = self::getConfig();
        $result = [];

        if(isset($cfg['service_manager']['filters']) && is_array($cfg['service_manager']['filters'])) {
            $array_object = json_decode(json_encode($object), true);
            foreach ($cfg['service_manager']['filters'] as $class_interface => $array_fields) {
                if(in_array($class_interface, $interfaces)) {
                    // структурирование выдачи по интерфейсам
                    $result[$class_interface] = array_intersect_key($array_object, array_fill_keys($array_fields, ''));
                }
            }
        }
        // если передан один интерфейс то возвращение без структурирования
        if(!empty($interface) && ((is_array($interface) && count($interface) == 1) || is_string($interface))) {
            $interface = is_array($interface)? reset($interface): $interface;
            return isset($result[$interface])?$result[$interface]:$result;
        }

        return $result;
    }
}
