<?php

/**
 * Microservice
 * 
 * @package    User
 * @version    1.0
 */

class User_Repository extends Db_Zend_Repository {

    public function getByGuid($guid){
       return $this->fetchItem( $this->select()->where(['sso_guid' => $guid]) );
    }

    /**
        SELECT `engine4_company_companies`.*
        FROM `engine4_company_companies`
        RIGHT JOIN `engine4_group_membership` ON `engine4_group_membership`.`resource_id` = `engine4_company_companies`.`group_id`
        WHERE (is_deleted = 0) AND ((is_private = 0))
        AND (`engine4_group_membership`.`user_id` = 237581)
        AND (`engine4_group_membership`.`active` = '1')
        AND (`is_company` = 1)
     **/

    public function getByCompany($company_id, $to_array = false) {

        //$this->sql->getAdapter()->driver->getConnection()->execute('SET CHARSET utf8;');

        if(!$company_id) {
            return false;
        }
        if(empty($role)) {
            $users = $this->fetchRows(
                $this->sql->select()
                    ->from(['e4cc' => 'engine4_company_companies'])
                    ->columns([], true)
                    ->join(['e4gm' => 'engine4_group_membership'], 'e4cc.group_id = e4gm.resource_id', [])
                    ->join(['e4u' => 'engine4_users'], 'e4u.user_id = e4gm.user_id', '*')
                    ->where(sprintf('e4cc.company_id = %d', $company_id))
            );
        }
        else {
            $users = $this->fetchRows(
                $this->sql->select()
                    ->from(['e4cc' => 'engine4_company_companies'])
                    ->columns([], true)
                    ->join(['e4gm' => 'engine4_group_membership'], 'e4cc.group_id = e4gm.resource_id', [])
                    ->join(['e4u' => 'engine4_users'], 'e4u.user_id = e4gm.user_id', '*')
                    ->where(sprintf('e4cc.company_id = %d', $company_id))
                    ->where('`is_company` = 1')
            );
        }

        if(is_array($users) && $to_array) {

            $result = [];
            foreach($users as $user_id => $user) {
                $result[$user_id] = $user;
            }

            return $result;
        }

        return $users;
    }


    public function getByGroup($group_id, $to_array = false) {
        if(!$group_id) {
            return false;
        }

        $users = $this->fetchItems(
            $this->sql->select()
                ->from(['e4cc' => 'engine4_group_groups'])
                ->columns([], true)
                ->join(['e4gm' => 'engine4_group_membership'], 'e4cc.group_id = e4gm.resource_id', [])
                ->join(['e4u' => 'engine4_users'], 'e4u.user_id = e4gm.user_id', '*')
                ->where(sprintf('e4cc.group_id = %d', $group_id))
                ->where('`is_company` = 0')
        );

        if(is_array($users) && $to_array) {
            $result = [];
            foreach($users as $user) {
                $result[] = $user->toArray();
            }

            return $result;
        }

        return $users;
    }

}
